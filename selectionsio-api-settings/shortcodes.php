<?php
function Selectionsioproduct_calculator_shortcode($atts) {

    $atts = shortcode_atts( array(
        'price' => '',
        'salebtn' => '',
        'description' => '',
        'calcid' => '',
        'measure' => '',
    ), $atts );

    $price = $atts['price'];
    $salebtn = $atts['salebtn'];
    $description = $atts['description'];
    $calcid = $atts['calcid'];
    $measure = $atts['measure'];
    $api_link = esc_attr( get_option('sio_api_link') );

    if($description != "0" AND $description != "") {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://printchampion.eu/apps/api/product/' . $calcid,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $result = json_decode($response);
    }

    global $wpdb;
    $table_name = $wpdb->prefix . "Selectionsioapi_imageplugin where `produkt_id` = '".$calcid."'";
    $results = $wpdb->get_results( "SELECT * FROM $table_name" ); 

    $table_name = $wpdb->prefix . "Selectionsioapi_cssplugin";
    $results2 = $wpdb->get_results( "SELECT * FROM $table_name" );


    $output = '<script type="text/javascript">';
    $output .= "apiseite = '$api_link';";
    $output .= "productUUId = '$calcid';";
    $output .= 'productLoaded = 0;';
    $output .= 'productValues = {};';
    $output .= "productLanguage = 'EUR';";
    $output .= 'productXml = "";';
    $output .= "productUrl = 'examen-thesis-din-a5';";
    $output .= "viewwk = 0;";
    $output .= "viewcheckout = 0;";
    $output .= "stylecss = \"" .preg_replace('/\s+/', ' ', $results2[0]->style). "\";";
    if($salebtn != "") {
        $output .= "salebtn = 1;";
    } else {
        $output .= "salebtn = 0;";
    }
    $output .= '</script>';
    if($description == "1") {
    $output .= '<div class="pscdescription">' . $result->description . '</div>';
    }
    $output .= '<div class="calcformdiv"><form id="CALCFORM" class=""></form></div>';
    $output .= '<div class="calcpricediv">';
    if($price != "" AND $measure == "") {
    $output .= '<table class="producttable">';
    $output .= '<tbody>';
    $output .= '<tr>';
    $output .= '<td>Preis (netto):</td>';
    $output .= '<td class="pull-right"><span id="netto"></span></td>';
    $output .= '<tr>
    <td>zzgl.  MwSt.:</td>
    <td class="pull-right"><span id="mwert"></span></td>
</tr>
<tr>
    <td><strong>Preis (brutto):</strong></td>
    <td class="pull-right"><strong><span id="brutto"></span></strong></td>
</tr>
</tbody>
</table>';
    } else if($price != "" AND $measure != "") {
        $output .= '<table class="producttable">';
    $output .= '<tbody>';
    $output .= '<tr>';
    $output .= '<td>' . $measure . ':</td>';
    $output .= '<td class="pull-right"><span id="netto"></span></td>';
    $output .= '<tr style="display:none">
    <td>zzgl.  MwSt.:</td>
    <td class="pull-right"><span id="mwert"></span></td>
</tr>
<tr style="display:none">
    <td><strong>Preis (brutto):</strong></td>
    <td class="pull-right"><strong><span id="brutto"></span></strong></td>
</tr>
</tbody>
</table>';
    }
    
    //if(isset($_GET["cart"])) {
    $output .= '<div id="productpic" style="display:none;">'.$results[0]->produkt_image_url.'</div><div id="productId" style="display:none;">'.$calcid.'</div><div id="nettoend" style="display:none;"></div><div id="mwertend" style="display:none;"></div><div id="bruttoend" style="display:none;"></div>';
    if($salebtn != "") {
    $output .= '<div class="salebuttondiv">';
    $output .= '<input type="submit" onclick="addtoCard();" id="add_to_cart" value="In den Warenkorb legen">';
    $output .= '</div>';
    }
    $output .= '</div>';
    //$output .= '<input type="hidden" name="upload_mode" value="none" id="upload_mode" /><div id="productId" style="display:none;">'.$calcid.'</div><div id="nettoend" style="display:none;"></div><div id="mwertend" style="display:none;"></div><div id="bruttoend" style="display:none;"></div><button id="in_basket">In den Warenkorb legen</button>';
    //}

    $output .= '<div class="shopping-cart"><a href="/warenkorb/"><span class="fa fa-shopping-cart" title="Warenkorb: (0) 0,00&nbsp;€"></span><span class="shopping-cart-data">Warenkorb: (<span id="shoppingcart-count">0</span>) <span id="shoppingcart-price">0,00 €</span></span></a></div><div class="floatclearfix"></div>';
    if($description == "2") {
        $output .= '<div class="pscdescription">' . $result->description . '</div>';
    }
    return $output;
}

function enqueue_calcwp_script() {
    wp_enqueue_script( 'calcwp-script', plugins_url( 'js/calcwp.js', __FILE__ ), array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_calcwp_script' );

function enqueue_cart_script() {
    wp_enqueue_script( 'cart', plugin_dir_url( __FILE__ ) . 'js/cart.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_cart_script' );
function enqueue_indexedDB_script() {
    wp_enqueue_script( 'indexedDB', plugin_dir_url( __FILE__ ) . 'js/indexedDB.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_indexedDB_script' );

add_shortcode( 'Selectionsioproduct_calculator', 'Selectionsioproduct_calculator_shortcode' );
