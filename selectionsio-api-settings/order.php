<?php
function Selectionsioapi_order_page() {
?>
<style>
.SIO-logo {
    position: absolute;
    right: 20px;
    top: 50%;
    margin-top: -60px;
    width: 313px;
    height: 80px;
    background: url(<?php echo plugins_url( 'images/selectionsio.png', __FILE__ ); ?>) center top/313px 63px no-repeat;
}
.SIO-version {
    position: absolute;
    width: 100%;
    bottom: 0;
    text-align: center;
    color: #72777c;
    line-height: 1em;
}
#textarea, #textarea2 {
    width: 400px;
}
</style>
<div class="wrap">
    <a target="_blank" href="https://Selectionsio.de/"><div class="SIO-logo">
			<div class="SIO-version">Selectionsio Produkt-Konfigurator v.1.0.0</div>
		</div></a>
        <h1>Selectionsio Auftragsübersicht</h1>
</div>
<?php } ?>