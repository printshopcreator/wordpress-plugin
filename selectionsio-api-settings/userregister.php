<?php
function register_via_api($username, $password) {
    $urlAPI = esc_attr( get_option('sio_api_link') ) . "/apps/api/contact/existswithpassword";
    $data = array(
      'username' => $username,
      'password' => $password
    );
    $options = array(
      'http' => array(
          'method' => 'POST',
          'content' => json_encode($data),
          'header' => "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n" .
              'apiKey: 8a49e32c-a0fd-4aa3-9e79-6d7c1a5587aa'
      )
    );
   
    $context = stream_context_create($options);
    $result = file_get_contents($urlAPI, false, $context);
    $response = json_decode($result);
   //var_dump($response);
   if ($response->exists) {
    $user = get_user_by('login', $_POST['self_email']);
    if (!$user) {
      $user = wp_create_user($_POST['self_email'], $_POST['password'], $_POST['self_email']);
    }
    if (!is_wp_error($user) && $user) {
        return true;
      } else {
        return false;
      }
  } else {
    ?>
Benutzername oder Passwort falsch.
    <?php
  }
}

  
function Selectionsio_my_register_form() {
    /*if (is_user_registered()) {
      return;
    }
    if (isset($_POST['self_email']) && isset($_POST['password'])) {
      $response = login_via_api($_POST['self_email'], $_POST['password']);
      if ($response) {
        $user = get_user_by('login', $_POST['self_email']);
        wp_set_current_user($user->ID, $_POST['self_email']);
        wp_set_auth_cookie($user->ID);
        do_action('wp_login', $_POST['self_email']);
        wp_redirect(home_url());
        exit;
      }
    }*/
    global $wpdb;
    $table_name = $wpdb->prefix . "Selectionsioapi_formplugin where `id` = 1";
    $results = $wpdb->get_results( "SELECT * FROM $table_name" );
    echo "<style>" .  $results[0]->formcssdata . "</style>";
    ?>
    <form action="" method="post">
<?php echo $results[0]->registerform; ?>
    </form>
    <?php
  }
  
  

  function Selectionsio_my_register_form_shortcode() {
    ob_start();
    Selectionsio_my_register_form();
    return ob_get_clean();
  }
  add_shortcode( 'Selectionsio_my_register_form', 'Selectionsio_my_register_form_shortcode' );
  