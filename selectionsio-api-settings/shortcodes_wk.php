<?php
function Selectionsio_wk_shortcode($atts) {
    $atts = shortcode_atts( array(
        'divid' => '',
    ), $atts );
    $divID = $atts['divid'];
    global $wpdb;
    $table_name = $wpdb->prefix . "Selectionsioapi_cssplugin";
    $results2 = $wpdb->get_results( "SELECT * FROM $table_name" );

    $output = '<script type="text/javascript">';
    $output .= "viewwk = 1;";
    $output .= "stylecss = \"" .preg_replace('/\s+/', ' ', $results2[0]->style). "\";";
    $output .= "viewcheckout = 0;";
    $output .= "divID = '$divID';";
    $output .= '</script>';
    $output .= '<div class="shopping-cart"><a href="/warenkorb/"><span class="fa fa-shopping-cart" title="Warenkorb: (0) 0,00&nbsp;€"></span><span class="shopping-cart-data">Warenkorb: (<span id="shoppingcart-count">0</span>) <span id="shoppingcart-price">0,00 €</span></span></a></div>';
    return $output;
}
add_shortcode( 'Selectionsio_wk', 'Selectionsio_wk_shortcode' );

function enqueue_cart_script_wk() {
    wp_enqueue_script( 'cart', plugin_dir_url( __FILE__ ) . 'js/cart.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_cart_script_wk' );
function enqueue_indexedDB_script_wk() {
    wp_enqueue_script( 'indexedDB', plugin_dir_url( __FILE__ ) . 'js/indexedDB.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_indexedDB_script_wk' );