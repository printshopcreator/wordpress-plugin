<?php
function my_render_settings_page(){
    if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
        $username = sanitize_text_field( $_POST['username'] );
        $email = sanitize_email( $_POST['email'] );
        $password = sanitize_text_field( $_POST['password'] );
        $password_wd = sanitize_text_field( $_POST['password_wd'] );
      
        $user_id = get_current_user_id();
        $user = get_user_by( 'id', $user_id );
        if($password != $password_wd) {
            echo '<p>Passwörter stimmen nicht überein!</p>';
        } else {
        $user_data = [
          'ID'        => $user_id,
          'user_login'  => $username,
          'user_email' => $email,
          'user_pass'   => $password,
        ];
        $user_id = wp_update_user( $user_data );
        if ( is_wp_error( $user_id ) ) {
          echo '<p>Fehler beim Aktualisieren des Benutzers: ' . $user_id->get_error_message() . '</p>';
        } else {
          echo '<p>Benutzer erfolgreich aktualisiert!</p>';
        }
        }
      }
      
    // Hier können Sie Ihre HTML-Markup, Formulare und PHP-Code hinzufügen, um die Seite zu rendern
    echo '<div class="wrap">';
    echo '<h1>Profil</h1>';
    echo '<h2>Persönliche Optionen</h2>';
    echo '</div>';
    $user_id = get_current_user_id();
    $user = get_user_by( 'id', $user_id );
    $password = "";
?>
<form action="" method="post">
  <p>
    <label for="username">Benutzername:</label>
    <input type="text" id="username" name="username" value="<?php echo $user->user_login; ?>" readonly>
  </p>
  <p>
    <label for="email">E-Mail-Adresse:</label>
    <input type="email" id="email" name="email" value="<?php echo $user->user_login; ?>">
  </p>
  <p>
    <label for="password">Passwort:</label>
    <input type="password" id="password" name="password" value="<?php echo $password; ?>" autocomplete="off">
  </p>
  <p>
    <label for="password">Passwort wiederholen::</label>
    <input type="password" id="password_wd" name="password_wd" value="<?php echo $password; ?>" autocomplete="off">
  </p>
  <p>
    <input type="submit" value="Speichern">
  </p>
</form>
<?php
}


function hide_profile_menu_items() {
  if ( !current_user_can( 'manage_options' ) ) {
      remove_menu_page( 'profile.php' );
      remove_submenu_page( 'users.php', 'profile.php' );
  }
}
add_action( 'admin_menu', 'hide_profile_menu_items', 999 );

function redirect_to_custom_profile_page() {
  if ( !current_user_can( 'manage_options' ) ) {
      wp_redirect( admin_url( 'admin.php?page=Selectionsioapi-usersetting' ) );
      exit;
  }
}
add_action( 'load-profile.php', 'redirect_to_custom_profile_page' );

function my_add_menu_items(){
  if ( !current_user_can( 'administrator' ) ) {
  add_menu_page(
      'Profil', // Seitentitel
      'Profil', // Menütitel
      'read', // Berechtigung
      'Selectionsioapi-usersetting', // Slug
      'my_render_settings_page', // Funktion, die die Seite rendert
      'dashicons-admin-users', // Icon
      80 // Position im Menü
  );
  add_menu_page(
    'Bestellungen', // Seitentitel
    'Bestellungen', // Menütitel
    'read', // Berechtigung
    'Selectionsioapi-userorders', // Slug
    'my_render_order_page', // Funktion, die die Seite rendert
    'dashicons-cart', // Icon
    85 // Position im Menü
);
}
}
add_action('admin_menu', 'my_add_menu_items');

function my_add_menu_items_site(){
  if ( !current_user_can( 'administrator' ) ) {
  add_menu_page( 
    'Logout', 
    'Logout', 
    'read', 
    wp_logout_url(), 
    '', 
    'dashicons-exit', 
    99 
  );
}
}
add_action('admin_menu', 'my_add_menu_items_site');

function remove_dashboard_link_for_non_admin() {
  if ( ! current_user_can( 'manage_options' ) ) {
      global $wp_admin_bar;
      $wp_admin_bar->remove_menu( 'dashboard' );
  }
}
add_action( 'wp_before_admin_bar_render', 'remove_dashboard_link_for_non_admin' );

function hide_dashboard_for_non_admin_users() {
  if (!current_user_can('manage_options')) {
    remove_menu_page('index.php');
  }
}
add_action('admin_menu', 'hide_dashboard_for_non_admin_users');

require_once( ABSPATH . 'wp-includes/pluggable.php' );

function remove_wp_logo( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'wp-logo' );
}

if (!current_user_can('administrator')) {
    add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
}

function remove_site_name( $wp_admin_bar ) {
  if ( !current_user_can( 'administrator' ) ) {
    $wp_admin_bar->remove_node( 'site-name' );
  }
}
add_action( 'admin_bar_menu', 'remove_site_name', 999 );

function my_admin_bar_profile_link( $admin_bar ) {
  if ( !current_user_can( 'administrator' ) ) {
    $current_user = wp_get_current_user();
    if ( 0 != $current_user->ID ) {
        $title = 'Mein Profil';
        $href = get_edit_profile_url( $current_user->ID );
        $icon = 'dashicons-admin-users';
        if ( strpos( $_SERVER['REQUEST_URI'], 'admin.php' ) ) {
            $title = 'Zur Startseite';
            $href = get_home_url();
            $icon = 'dashicons-admin-home';
        }
        $admin_bar->add_menu( array(
            'id'    => 'my-profile',
            'title' => '<span class="ab-icon '.$icon.'"></span>' . $title,
            'href'  => $href,
            'meta'  => array(
                'title' => __( $title ),
            ),
        ) );
    }
  }
}
add_action( 'admin_bar_menu', 'my_admin_bar_profile_link', 90 );

function auto_redirect_after_logout(){
  wp_redirect( home_url() );
  exit();
}
add_action( 'wp_logout', 'auto_redirect_after_logout' );
 ?>