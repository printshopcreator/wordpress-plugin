var $ = jQuery.noConflict();
let cartArray = [];
let arrayin = true;
let cellCount = 0;
let wkCount = 0;
let wkBrutto = 0;

$(document).ready(function(){
  console.log("TEST");
var highest = 0;
var keys = [];
for (var i = 0; i < localStorage.length; i++) {
  var key = localStorage.key(i);
  if (key.startsWith("wk_")) {
    wkCount++;
    keys.push(key);
    var number = parseInt(key.slice(3), 10);
    if (number > highest) {
      highest = number;
    }
  }
}

keys.sort();

for (var i = 0; i < keys.length; i++) {
  var key = keys[i];
  console.log(key + " = " + JSON.stringify(localStorage.getItem(key)));
  var number = parseInt(key.slice(3), 10);
  localStorage.setItem("aktcellCount", number);
  var cartData = JSON.parse(localStorage.getItem(key));
  createCart(cartData, false);
}
cellCount = parseInt(highest)+1;
console.log("next number: " + cellCount);
localStorage.setItem("cellCount", cellCount);
});
// Add an event listener to the add to cart button
function addtoCard() {
    // Get the product ID
    var productId = document.getElementById('productId').innerText;
    var productpic = document.getElementById('productpic').innerText;

    // Serialize the form data
    var formData = new FormData(document.getElementById("CALCFORM"));
    var formDataJson = JSON.stringify(Object.fromEntries(formData));

    // Get the netto price
    var netto = document.getElementById('nettoend').innerText;
    var mwert = document.getElementById('mwertend').innerText;
    var brutto = document.getElementById('bruttoend').innerText;

        // Create a new cart in the local storage
        localStorage.setItem('cart', JSON.stringify({
            [productId]: {
                productId: productId,
                productpic: productpic,
                formData: formDataJson,
                netto: netto,
                mwert: mwert,
                brutto: brutto
            }
        }));
    //console.log(localStorage.getItem('cart'));
    wkCount++;
    createCart(localStorage.getItem('cart'), true);
}
let totalNetto = 0;
let totalVat = 0;
let totalBrutto = 0;
let priceNetto = 0;
let priceVat = 0;
let priceBrutto = 0;
function createCart(data, arrayin) {
    if (getCookie('cartItems') != "") {
        setCookie('cartItems', '', -1);
    }
    // Parse the data
    let cartData = JSON.parse(data);
if(arrayin == true) {
    cartArray.push(data);
}
    //console.log(cartArray);
for (let productId in cartData) {
  priceBrutto = cartData[productId].brutto;
  wkBrutto = parseFloat(wkBrutto)+parseFloat(priceBrutto);
}
if (typeof viewwk !== 'undefined' && typeof viewwk !== 'undefined') 
if(viewwk == 1 || viewcheckout == 1) {
    // Create our container
let container = document.createElement('div');
container.classList.add('cart-container');
container.setAttribute("id", "cart-container" + cellCount)

// Create our table
let table = document.createElement('div');
table.classList.add('cart-table');
if(cellCount == 0) {
// Create our header row
//let headerRow = document.createElement('tr');
//headerRow.classList.add('cart-table-header');
//headerRow.setAttribute("id", "head" + cellCount);
let productIdHeader = document.createElement('div');
productIdHeader.innerHTML = 'Produktbild';
productIdHeader.setAttribute("class", "productpic_header");
let descriptionHeader = document.createElement('div');
descriptionHeader.innerHTML = 'Beschreibung';
descriptionHeader.setAttribute("class", "productdescription_header");
let priceHeader = document.createElement('div');
priceHeader.innerHTML = 'Preis';
priceHeader.setAttribute("class", "productprice_header");
// Append the headers to the row
table.appendChild(productIdHeader);
table.appendChild(descriptionHeader);
table.appendChild(priceHeader);
if(viewcheckout != 1) {
let deleteHeader = document.createElement('div');
deleteHeader.innerHTML = 'Löschen';
deleteHeader.setAttribute("class", "productdelete_header");
table.appendChild(deleteHeader);
}
// Append the header row to the table
//table.appendChild(headerRow);
let containerClearfix = document.createElement('div');
containerClearfix.classList.add('clearfix');
containerClearfix.setAttribute("id", "clearfix_header");
containerClearfix.setAttribute("style", "clear: both;");
table.appendChild(containerClearfix);
}
// Append each product to the table
for (let productId in cartData) {
  //let productRow = document.createElement('tr');
  //productRow.classList.add('cart-table-product');
  //productRow.setAttribute("id", "cell" + cellCount);
  let productIdCell = document.createElement('div');
  productIdCell.setAttribute("style", "text-align:center");
  productIdCell.setAttribute("class", "productpic_body");
  productIdCell.innerHTML = "<img src=" + cartData[productId].productpic + ">";
  let descriptionCell = document.createElement('div');
  descriptionCell.setAttribute("class", "productdescription_body");
  let deleteCell = document.createElement('div');
  deleteCell.setAttribute("class", "productdelete_body");
  let formData = cartData[productId].formData;
  let formDataout = JSON.parse(formData);
  //console.log(formDataout);
  for (let key in formDataout) {
      let value = formDataout[key];
      descriptionCell.innerHTML += `${key}: ${value}<br>`;
  }
  let priceCell = document.createElement('div');
  priceCell.setAttribute("class", "productprice_body");
  priceNetto = cartData[productId].netto;
  priceVat = cartData[productId].mwert;
  priceBrutto = cartData[productId].brutto;
  let totalNettoProduct = priceNetto;
  let totalVatProduct = priceVat;
  let totalBruttoProduct = priceBrutto;
  let productLanguage = 'EUR';
  priceCell.innerHTML = 'Gesamtnetto: ' + new Intl.NumberFormat('de-DE', { style: 'currency', currency: productLanguage }).format(totalNettoProduct) + "<br />" + 'MwSt: ' + new Intl.NumberFormat('de-DE', { style: 'currency', currency: productLanguage }).format(totalVatProduct) + "<br />" + 'Gesamtbrutto: ' + new Intl.NumberFormat('de-DE', { style: 'currency', currency: productLanguage }).format(totalBruttoProduct);
  table.appendChild(productIdCell);
  table.appendChild(descriptionCell);
  table.appendChild(priceCell);
  if(viewcheckout != 1) {
  deleteCell.innerHTML = '<label style="text-decoration: underline;cursor: pointer;" onclick="deleteCell('+localStorage.getItem("aktcellCount")+');">Löschen</label> ';
  table.appendChild(deleteCell);
  }
 

  //table.appendChild(productRow);
}

// Append the table to the container
container.appendChild(table);
let containerClearfix = document.createElement('div');
containerClearfix.classList.add('clearfix');
containerClearfix.setAttribute("style", "clear: both;");
table.appendChild(containerClearfix);
// Append the container to the body
//document.body.appendChild(container);

var vorhandenesObjekt = document.getElementById(divID);
vorhandenesObjekt.appendChild(container);
// Create a total row
totalPrice(cartArray);
}
let cartItemsString = JSON.stringify(data);
if(arrayin == true) {
localStorage.setItem("wk_" + localStorage.getItem("cellCount"), cartItemsString);
var highest = 0;
for (var i = 0; i < localStorage.length; i++) {
  var key = localStorage.key(i);
  if (key.startsWith("wk_")) {
    var number = parseInt(key.slice(3), 10);
    if (number > highest) {
      highest = number;
    }
  }
}
cellCount = parseInt(highest)+1;
console.log("next number: " + cellCount);
localStorage.setItem("cellCount", cellCount);
}
if (typeof viewwk !== 'undefined' && typeof viewwk !== 'undefined') 
if(viewcheckout == 0 && viewwk == 0) {
  console.log(wkCount);
  console.log(wkBrutto);
  document.getElementById('shoppingcart-count').innerText = wkCount;
  document.getElementById('shoppingcart-price').innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: productLanguage }).format(wkBrutto);
}
cellCount = cellCount+1;
}

function totalPrice(tableRows) {
$( ".cart-total-container" ).empty();
let containerTotal = document.createElement('div');
containerTotal.classList.add('cart-total-container');

// Create our table
let tableTotal = document.createElement('table');
tableTotal.classList.add('cart-total-table');

// Create a total row
let totalRow = document.createElement('tr');
let totalCell = document.createElement('td');
totalCell.colSpan = "3";
let productLanguage = "EUR";
if(totalNetto != 0) {
    totalNetto = parseFloat(totalNetto)+parseFloat(priceNetto);
    totalVat = parseFloat(totalVat)+parseFloat(priceVat);
    totalBrutto = parseFloat(totalBrutto)+parseFloat(priceBrutto);
} else {
    totalNetto = priceNetto;
    totalVat = priceVat;
    totalBrutto = priceBrutto;
}
totalCell.innerHTML = 'Gesamtnetto: ' + new Intl.NumberFormat('de-DE', { style: 'currency', currency: productLanguage }).format(totalNetto) + "<br />" + 'MwSt: ' + new Intl.NumberFormat('de-DE', { style: 'currency', currency: productLanguage }).format(totalVat) + "<br />" + '<label id="endprice">Gesamtbrutto: ' + new Intl.NumberFormat('de-DE', { style: 'currency', currency: productLanguage }).format(totalBrutto) + "</label>";
document.getElementById('shoppingcart-count').innerText = wkCount;
document.getElementById('shoppingcart-price').innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: productLanguage }).format(totalBrutto);
totalRow.appendChild(totalCell);
tableTotal.appendChild(totalRow);
// Append the table to the container
containerTotal.appendChild(tableTotal);

let containerClearfix = document.createElement('div');
containerClearfix.classList.add('clearfix');
containerClearfix.setAttribute("style", "clear: both;");
containerTotal.appendChild(containerClearfix);
// Create a button to finish the checkout
if(viewcheckout != 1) {
/*let checkoutBtn = document.createElement('button');
checkoutBtn.classList.add('checkout-btn');
checkoutBtn.innerHTML = 'Kasse';

// Add an event listener to the button
checkoutBtn.addEventListener("click", function() {
    if (getCookie('cartItems') != "") {
        setCookie('cartItems', '', -1);
    }
   // var cartDataJson = JSON.stringify(tableRows);
   for (let allData in tableRows) {
    let value = tableRows[allData];
  console.log(allData);
  console.log(`${value}`);
   }

// Array in einen String umwandeln
let cartItemsString = JSON.stringify(tableRows);

// Cookie erstellen
//setCookie("cartItems", cartItemsString, 7); // Speichert das Cookie für 7 Tage

    // Speichere den JSON-String in einem Cookie
    //document.cookie = "cart=" + cartDataJson + "; expires=Thu, 18 Dec 2023 12:00:00 UTC; path=/";
  });
  */
// Append the button to the container
//containerTotal.appendChild(checkoutBtn);
}
// Append the container to the body
//document.body.appendChild(containerTotal);

var vorhandenesObjekt = document.getElementById(divID);
vorhandenesObjekt.appendChild(containerTotal);
}
function deleteCell(id) {
const elements1 = document.getElementsByClassName("cart-container");
while (elements1.length > 0) elements1[0].remove();
const elements2 = document.getElementsByClassName("cart-total-container");
while (elements2.length > 0) elements2[0].remove();
const elements3 = document.getElementsByClassName("cart-total-table");
while (elements3.length > 0) elements3[0].remove();
localStorage.removeItem("wk_" + id);
    totalNetto = 0;
    totalVat = 0;
    totalBrutto = 0;
    priceNetto = 0;
    priceVat = 0;
    priceBrutto = 0;
    var highest = 0;
    var keys = [];
    for (var i = 0; i < localStorage.length; i++) {
      var key = localStorage.key(i);
      if (key.startsWith("wk_")) {
        keys.push(key);
        var number = parseInt(key.slice(3), 10);
        if (number > highest) {
          highest = number;
        }
      }
    }
    
    keys.sort();
    
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      console.log(key + " = " + JSON.stringify(localStorage.getItem(key)));
      var number = parseInt(key.slice(3), 10);
      localStorage.setItem("aktcellCount", number);
      var cartData = JSON.parse(localStorage.getItem(key));
      createCart(cartData, false);
    }
    cellCount = parseInt(highest)+1;
    console.log("next number: " + cellCount);
    localStorage.setItem("cellCount", cellCount);
}
function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

// Add a stylesheet for some basic styling
let stylesheet = document.createElement('style');
stylesheet.setAttribute("id", "style_warehouse");
if (typeof stylecss !== 'undefined') 
stylesheet.innerHTML = stylecss;
document.head.appendChild(stylesheet);