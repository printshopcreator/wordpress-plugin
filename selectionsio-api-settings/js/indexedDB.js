// Überprüfen, ob IndexedDB unterstützt wird
// Überprüfung, ob IndexedDB unterstützt wird
function checkIDBSupport() {
    return "indexedDB" in window;
  }
  
  // Öffnen der Datenbank
  function openDB(dbName, version) {
    return new Promise((resolve, reject) => {
      let request = indexedDB.open(dbName, version);
  
      request.onsuccess = event => {
        console.log(`Datenbank "${dbName}" erfolgreich geöffnet.`);
        //resolve(event.target.result);
        let db = event.target.result;
        createObjectStore(db, "MeinObjektstore", "id").then(() => {
            console.log(`Datenbank "${dbName}" wurde aktualisiert.`);
          });
      };
  
      request.onerror = event => {
        console.error(`Fehler beim Öffnen der Datenbank "${dbName}":`, event.target.error);
        reject(event.target.error);
      };
  
      request.onupgradeneeded = event => {
        let db = event.target.result;
  
        createObjectStore(db, "MeinObjektstore", "id").then(() => {
          console.log(`Datenbank "${dbName}" wurde aktualisiert.`);
        });
      };
    });
  }
  
  // Anlegen eines Objektstores mit auto-increment
  function createObjectStore(db, storeName, keyPath) {
    return new Promise((resolve, reject) => {
      let transaction = db.transaction([storeName], "readwrite");
  
      transaction.oncomplete = event => {
        console.log(`Objektstore "${storeName}" erfolgreich angelegt.`);
        resolve();
      };
  
      transaction.onerror = event => {
        console.error(`Fehler beim Anlegen des Objektstores "${storeName}":`, event.target.error);
        reject(event.target.error);
      };
  
      let objectStore = transaction.objectStore(storeName);
      objectStore.createIndex(keyPath, keyPath, { autoIncrement: true });
    });
  }
  
  // Hinzufügen von Daten in den Objektstore
  function addData(db, storeName, data) {
    return new Promise((resolve, reject) => {
      let transaction = db.transaction([storeName], "readwrite");
  
      transaction.oncomplete = event => {
        console.log("Daten erfolgreich hinzugefügt.");
        resolve();
      };
  
      transaction.onerror = event => {
        console.error("Fehler beim Hinzufügen der Daten:", event.target.error);
        reject(event.target.error);
      };
  
      let objectStore = transaction.objectStore(storeName);
      let request = objectStore.add(data);
    });
  }
  
  

  // Funktion zum Finden von Daten in der Datenbank
function findData(db, storeName, key) {
    let transaction = db.transaction([storeName], "readonly");
    let objectStore = transaction.objectStore(storeName);
    let request = objectStore.get(key);
    
    request.onsuccess = function() {
      // Ausgabe des gefundenen Datensatzes
      console.log("Datensatz gefunden: ", request.result);
    };
    
    request.onerror = function() {
      console.error("Fehler beim Finden des Datensatzes: ", request.error);
    };
  }
  
  // Funktion zum Löschen von Daten aus der Datenbank
  function deleteData(db, storeName, key) {
    let transaction = db.transaction([storeName], "readwrite");
    let objectStore = transaction.objectStore(storeName);
    let request = objectStore.delete(key);
    
    request.onsuccess = function() {
      console.log("Datensatz erfolgreich gelöscht");
    };
    
    request.onerror = function() {
      console.error("Fehler beim Löschen des Datensatzes: ", request.error);
    };
  }
  
  // Funktion zum Auslesen aller Daten aus der Datenbank
  function getAllData(db, storeName) {
    let transaction = db.transaction([storeName], "readonly");
    let objectStore = transaction.objectStore(storeName);
    let request = objectStore.getAll();
    
    request.onsuccess = function() {
      console.log("Alle Datensätze ausgelesen: ", request.result);
    };
    
    request.onerror = function() {
      console.error("Fehler beim Auslesen aller Datensätze: ", request.error);
    };
  }

  // Funktion zum Auslesen von Daten mit Index-Schlüssel
function getDataWithIndex(indexName, value) {
    let objectStore = db.transaction(storeName).objectStore(storeName);
    let index = objectStore.index(indexName);
    let request = index.get(value);
    
    request.onsuccess = function(event) {
      console.log("Daten erfolgreich ausgelesen mit Index " + indexName + " und Wert " + value + ":", event.target.result);
    };
    
    request.onerror = function(event) {
      console.error("Fehler beim Auslesen von Daten mit Index " + indexName + " und Wert " + value + ":", event.target.error);
    };
  }
  
  // Funktion zum Löschen aller Daten
  function deleteAllData() {
    let objectStore = db.transaction(storeName, "readwrite").objectStore(storeName);
    let request = objectStore.clear();
    
    request.onsuccess = function(event) {
      console.log("Alle Daten erfolgreich gelöscht.");
    };
    
    request.onerror = function(event) {
      console.error("Fehler beim Löschen aller Daten:", event.target.error);
    };
  }

  // Funktion zum Löschen einer Datenbank
function deleteDB() {
    var request = window.indexedDB.deleteDatabase("MyTestDB");
    request.onsuccess = function() {
        console.log("Datenbank erfolgreich gelöscht.");
    };
    request.onerror = function() {
        console.log("Fehler beim Löschen der Datenbank.");
    };
    request.onblocked = function() {
        console.log("Löschen der Datenbank wurde blockiert.");
    };
}

// Verwendung:
// Aufrufen der deleteDB()-Funktion, um eine vorhandene Datenbank zu löschen.

// Funktion zum Auslesen der Version einer IndexedDB-Datenbank
function getDBVersion(dbName) {
    // Überprüfen, ob IndexedDB unterstützt wird
    if (!window.indexedDB) {
      console.error("IndexedDB wird nicht unterstützt");
      return;
    }
  
    // Auslesen der Version einer bestehenden Datenbank
    var request = window.indexedDB.open(dbName);
    request.onsuccess = function(event) {
      var db = event.target.result;
      console.log("Die aktuelle Version der Datenbank ist:", db.version);
    };
  }
  
  // Benutzung der Funktion:
  // getDBVersion("meineDatenbank");
  
  // Funktion zur Optimierung der Datenbank
// verwendet die IDBFactory-Methode "deleteDatabase"
function optimierenDatenbank(dbName) {
    if (!window.indexedDB) {
      console.log("IndexedDB wird in diesem Browser nicht unterstützt.");
      return;
    }
  
    // Löschen der bestehenden Datenbank
    var request = window.indexedDB.deleteDatabase(dbName);
  
    // Überwachung des Löschvorgangs
    request.onsuccess = function() {
      console.log("Datenbank erfolgreich gelöscht.");
  
      // Erstellen einer neuen, optimierten Datenbank
      var request = window.indexedDB.open(dbName, 1);
      request.onsuccess = function(event) {
        console.log("Datenbank erfolgreich optimiert.");
      };
    };
  
    request.onerror = function(event) {
      console.log("Fehler beim Löschen der Datenbank: " + event.target.errorCode);
    };
  }
  
  // Verwendung der Funktion
  // optimierenDatenbank("meineDatenbank");
  