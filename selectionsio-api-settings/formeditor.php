<?php
function Selectionsioapi_formeditor_page() {
  global $wpdb;
  if(isset($_POST["formdatas"])) {
    if(isset($_POST["tab"]) AND $_POST["tab"] == "registerform") {
      $insertformdata = $_POST["formdatas"];
      $insertcssdata = $_POST["cssdatas"];
      $wpdb->update("{$wpdb->prefix}Selectionsioapi_formplugin", array(
        'registerform' => str_replace('\"', '"', $insertformdata),
        'formcssdata' => str_replace('\"', '"', $insertcssdata)
      ), array(
        'id' => 1
      ));
      $wpdb->print_error();
    }
    if(isset($_POST["tab"]) AND $_POST["tab"] == "deliveryandinvoiceform") {
      $insertformdata = $_POST["formdatas"];
      $insertcssdata = $_POST["cssdatas"];
      $wpdb->update("{$wpdb->prefix}Selectionsioapi_formplugin", array(
        'deliveryandinvoiceform' => str_replace('\"', '"', $insertformdata),
        'formcssdata' => str_replace('\"', '"', $insertcssdata)
      ), array(
        'id' => 1
      ));
      $wpdb->print_error();
    }
    if(isset($_POST["tab"]) AND $_POST["tab"] == "loginform") {
      $insertformdata = $_POST["formdatas"];
      $insertcssdata = $_POST["cssdatas"];
      $wpdb->update("{$wpdb->prefix}Selectionsioapi_formplugin", array(
        'loginform' => str_replace('\"', '"', $insertformdata),
        'formcssdata' => str_replace('\"', '"', $insertcssdata)
      ), array(
        'id' => 1
      ));
      $wpdb->print_error();
    }
    if(isset($_POST["tab"]) AND $_POST["tab"] == "pwlostform") {
      $insertformdata = $_POST["formdatas"];
      $insertcssdata = $_POST["cssdatas"];
      $wpdb->update("{$wpdb->prefix}Selectionsioapi_formplugin", array(
        'pwlostform' => str_replace('\"', '"', $insertformdata),
        'formcssdata' => str_replace('\"', '"', $insertcssdata)
      ), array(
        'id' => 1
      ));
      $wpdb->print_error();
    }
  }
  if((isset($_GET["tab"]) AND $_GET["tab"] == "registerform")) {
  $table_name = $wpdb->prefix . "Selectionsioapi_formplugin where `id` = 1";
  $results = $wpdb->get_results( "SELECT * FROM $table_name" );
  if($results[0]->registerform != "firstdata") {
    $outputformdata = $results[0]->registerform;
  } else {
    $outputformdata = "";
  }
  if($results[0]->formcssdata != "firstdata") {
    $outputcssdata = $results[0]->formcssdata;
  } else {
    $outputcssdata = "";
  }
  }

  if((isset($_GET["tab"]) AND $_GET["tab"] == "deliveryandinvoiceform")) {
    $table_name = $wpdb->prefix . "Selectionsioapi_formplugin where `id` = 1";
    $results = $wpdb->get_results( "SELECT * FROM $table_name" );
    if($results[0]->deliveryandinvoiceform != "firstdata") {
      $outputformdata = $results[0]->deliveryandinvoiceform;
    } else {
      $outputformdata = "";
    }
    if($results[0]->formcssdata != "firstdata") {
      $outputcssdata = $results[0]->formcssdata;
    } else {
      $outputcssdata = "";
    }
    }
    if((isset($_GET["tab"]) AND $_GET["tab"] == "loginform")) {
      $table_name = $wpdb->prefix . "Selectionsioapi_formplugin where `id` = 1";
      $results = $wpdb->get_results( "SELECT * FROM $table_name" );
      if($results[0]->loginform != "firstdata") {
        $outputformdata = $results[0]->loginform;
      } else {
        $outputformdata = "";
      }
      if($results[0]->formcssdata != "firstdata") {
        $outputcssdata = $results[0]->formcssdata;
      } else {
        $outputcssdata = "";
      }
      }

      if((isset($_GET["tab"]) AND $_GET["tab"] == "pwlostform")) {
        $table_name = $wpdb->prefix . "Selectionsioapi_formplugin where `id` = 1";
        $results = $wpdb->get_results( "SELECT * FROM $table_name" );
        if($results[0]->pwlostform != "firstdata") {
          $outputformdata = $results[0]->pwlostform;
        } else {
          $outputformdata = "";
        }
        if($results[0]->formcssdata != "firstdata") {
          $outputcssdata = $results[0]->formcssdata;
        } else {
          $outputcssdata = "";
        }
        }

  $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general';
  $laender['DE'] = "Deutschland"; 
  $laender['AF'] = "Afghanistan"; 
  $laender['AL'] = "Albanien"; 
  $laender['AS'] = "Amerikanisch Samoa"; 
  $laender['AD'] = "Andorra"; 
  $laender['AO'] = "Angola"; 
  $laender['AI'] = "Anguilla"; 
  $laender['AQ'] = "Antarktis"; 
  $laender['AG'] = "Antigua und Barbuda"; 
  $laender['AR'] = "Argentinien"; 
  $laender['AM'] = "Armenien"; 
  $laender['AW'] = "Aruba"; 
  $laender['AT'] = "Österreich"; 
  $laender['AU'] = "Australien"; 
  $laender['AZ'] = "Aserbaidschan"; 
  $laender['BS'] = "Bahamas"; 
  $laender['BH'] = "Bahrain"; 
  $laender['BD'] = "Bangladesh"; 
  $laender['BB'] = "Barbados"; 
  $laender['BY'] = "Weißrussland"; 
  $laender['BE'] = "Belgien"; 
  $laender['BZ'] = "Belize"; 
  $laender['BJ'] = "Benin"; 
  $laender['BM'] = "Bermuda"; 
  $laender['BT'] = "Bhutan"; 
  $laender['BO'] = "Bolivien"; 
  $laender['BA'] = "Bosnien Herzegowina"; 
  $laender['BW'] = "Botswana"; 
  $laender['BV'] = "Bouvet Island"; 
  $laender['BR'] = "Brasilien"; 
  $laender['BN'] = "Brunei Darussalam"; 
  $laender['BG'] = "Bulgarien"; 
  $laender['BF'] = "Burkina Faso"; 
  $laender['BI'] = "Burundi"; 
  $laender['KH'] = "Kambodscha"; 
  $laender['CM'] = "Kamerun"; 
  $laender['CA'] = "Kanada"; 
  $laender['CV'] = "Kap Verde"; 
  $laender['KY'] = "Cayman Inseln"; 
  $laender['CF'] = "Zentralafrikanische Republik"; 
  $laender['TD'] = "Tschad"; 
  $laender['CL'] = "Chile"; 
  $laender['CN'] = "China"; 
  $laender['CO'] = "Kolumbien"; 
  $laender['KM'] = "Comoros"; 
  $laender['CG'] = "Kongo"; 
  $laender['CK'] = "Cook Inseln"; 
  $laender['CR'] = "Costa Rica"; 
  $laender['CI'] = "Elfenbeinküste"; 
  $laender['HR'] = "Kroatien"; 
  $laender['CU'] = "Kuba"; 
  $laender['CZ'] = "Tschechien"; 
  $laender['DK'] = "Dänemark"; 
  $laender['DJ'] = "Djibouti"; 
  $laender['DO'] = "Dominikanische Republik"; 
  $laender['TP'] = "Osttimor"; 
  $laender['EC'] = "Ecuador"; 
  $laender['EG'] = "Ägypten"; 
  $laender['SV'] = "El Salvador"; 
  $laender['GQ'] = "Äquatorial Guinea"; 
  $laender['ER'] = "Eritrea"; 
  $laender['EE'] = "Estland"; 
  $laender['ET'] = "Äthiopien"; 
  $laender['FK'] = "Falkland Inseln"; 
  $laender['FO'] = "Faroe Inseln"; 
  $laender['FJ'] = "Fiji"; 
  $laender['FI'] = "Finland"; 
  $laender['FR'] = "Frankreich"; 
  $laender['GF'] = "Französisch Guiana"; 
  $laender['PF'] = "Französisch Polynesien"; 
  $laender['GA'] = "Gabon"; 
  $laender['GM'] = "Gambia"; 
  $laender['GE'] = "Georgien"; 
  $laender['GH'] = "Ghana"; 
  $laender['GI'] = "Gibraltar"; 
  $laender['GR'] = "Griechenland"; 
  $laender['GL'] = "Grönland"; 
  $laender['GD'] = "Grenada"; 
  $laender['GP'] = "Guadeloupe"; 
  $laender['GU'] = "Guam"; 
  $laender['GT'] = "Guatemala"; 
  $laender['GN'] = "Guinea"; 
  $laender['GY'] = "Guyana"; 
  $laender['HT'] = "Haiti"; 
  $laender['VA'] = "Vatikan"; 
  $laender['HN'] = "Honduras"; 
  $laender['HU'] = "Ungarn"; 
  $laender['IS'] = "Island"; 
  $laender['IN'] = "Indien"; 
  $laender['ID'] = "Indonesien"; 
  $laender['IR'] = "Iran"; 
  $laender['IQ'] = "Irak"; 
  $laender['IE'] = "Irland"; 
  $laender['IL'] = "Israel"; 
  $laender['IT'] = "Italien"; 
  $laender['JM'] = "Jamaika"; 
  $laender['JP'] = "Japan"; 
  $laender['JO'] = "Jordanien"; 
  $laender['KZ'] = "Kasachstan"; 
  $laender['KE'] = "Kenia"; 
  $laender['KI'] = "Kiribati"; 
  $laender['KW'] = "Kuwait"; 
  $laender['KG'] = "Kirgistan"; 
  $laender['LA'] = "Laos"; 
  $laender['LV'] = "Lettland"; 
  $laender['LB'] = "Libanon"; 
  $laender['LS'] = "Lesotho"; 
  $laender['LI'] = "Liechtenstein"; 
  $laender['LT'] = "Litauen"; 
  $laender['LU'] = "Luxemburg"; 
  $laender['MO'] = "Macau"; 
  $laender['MK'] = "Mazedonien"; 
  $laender['MG'] = "Madagaskar"; 
  $laender['MW'] = "Malawi"; 
  $laender['MY'] = "Malaysia"; 
  $laender['MV'] = "Malediven"; 
  $laender['ML'] = "Mali"; 
  $laender['MT'] = "Malta"; 
  $laender['MR'] = "Mauretanien"; 
  $laender['MU'] = "Mauritius"; 
  $laender['YT'] = "Mayotte"; 
  $laender['MX'] = "Mexiko"; 
  $laender['FM'] = "Mikronesien"; 
  $laender['MD'] = "Moldavien"; 
  $laender['MC'] = "Monaco"; 
  $laender['MN'] = "Mongolei"; 
  $laender['MS'] = "Montserrat"; 
  $laender['MA'] = "Marokko"; 
  $laender['MZ'] = "Mosambik"; 
  $laender['MM'] = "Myanmar"; 
  $laender['NA'] = "Namibia"; 
  $laender['NR'] = "Nauru"; 
  $laender['NP'] = "Nepal"; 
  $laender['NL'] = "Niederlande"; 
  $laender['NZ'] = "Neuseeland"; 
  $laender['NI'] = "Nicaragua"; 
  $laender['NE'] = "Niger"; 
  $laender['NG'] = "Nigeria"; 
  $laender['NU'] = "Niue"; 
  $laender['NF'] = "Norfolk Inseln"; 
  $laender['KP'] = "Nord Korea"; 
  $laender['NO'] = "Norwegen"; 
  $laender['OM'] = "Oman"; 
  $laender['PK'] = "Pakistan"; 
  $laender['PW'] = "Palau"; 
  $laender['PA'] = "Panama"; 
  $laender['PG'] = "Papua Neu Guinea"; 
  $laender['PY'] = "Paraguay"; 
  $laender['PE'] = "Peru"; 
  $laender['PH'] = "Philippinen"; 
  $laender['PL'] = "Polen"; 
  $laender['PT'] = "Portugal"; 
  $laender['PR'] = "Puerto Rico"; 
  $laender['RO'] = "Rumänien"; 
  $laender['RU'] = "Russland"; 
  $laender['RW'] = "Ruanda"; 
  $laender['WS'] = "Samoa"; 
  $laender['SM'] = "San Marino"; 
  $laender['SA'] = "Saudi-Arabien"; 
  $laender['SN'] = "Senegal"; 
  $laender['SRB'] = "Serbien"; 
  $laender['SC'] = "Seychellen"; 
  $laender['SL'] = "Sierra Leone"; 
  $laender['SG'] = "Singapur"; 
  $laender['SK'] = "Slovakei"; 
  $laender['SB'] = "Solomon Inseln"; 
  $laender['SO'] = "Somalia"; 
  $laender['ZA'] = "Südafrika"; 
  $laender['KR'] = "Südkorea"; 
  $laender['ES'] = "Spanien"; 
  $laender['LK'] = "Sri Lanka"; 
  $laender['SD'] = "Sudan"; 
  $laender['SR'] = "Suriname"; 
  $laender['SZ'] = "Swasiland"; 
  $laender['SE'] = "Schweden"; 
  $laender['CH'] = "Schweiz"; 
  $laender['SY'] = "Syrien"; 
  $laender['TW'] = "Taiwan"; 
  $laender['TJ'] = "Tadschikistan"; 
  $laender['TZ'] = "Tansania"; 
  $laender['TH'] = "Thailand"; 
  $laender['TG'] = "Togo"; 
  $laender['TO'] = "Tonga"; 
  $laender['TT'] = "Trinidad und Tobago"; 
  $laender['TN'] = "Tunesien"; 
  $laender['TR'] = "Türkei"; 
  $laender['TM'] = "Turkmenistan"; 
  $laender['TV'] = "Tuvalu"; 
  $laender['UG'] = "Uganda"; 
  $laender['UA'] = "Ukraine"; 
  $laender['AE'] = "Vereinigte Arabische Emirate"; 
  $laender['GB'] = "Vereinigtes Königreich"; 
  $laender['US'] = "Vereinigte Staaten von Amerika"; 
  $laender['UY'] = "Uruguay"; 
  $laender['UZ'] = "Usbekistan"; 
  $laender['VU'] = "Vanuatu"; 
  $laender['VE'] = "Venezuela"; 
  $laender['VN'] = "Vietnam"; 
  $laender['VG'] = "Virgin Islands"; 
  $laender['EH'] = "Westsahara"; 
  $laender['YE'] = "Jemen"; 
  $laender['YU'] = "Jugoslavien"; 
  $laender['ZR'] = "Zaire"; 
  $laender['ZM'] = "Sambia"; 
  $laender['ZW'] = "Simbabwe"; 
  $laender['SI'] = "Slowenien"; 
  $laender['XI'] = "Nordirland";

  $europeanCountries = array('DE', 'GB', 'AT', 'BE', 'BG', 'CY', 'CZ', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HU', 'HR', 'IE', 'IT', 'LT', 'LU', 'LV', 'MC', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SK', 'SI', 'XI');

  $anredearray = array(
    "Herr" => "Herr",
    "Frau" => "Frau",
    "Firma" => "Firma",
    "Herr Dr." => "Herr Dr.",
    "Frau Dr." => "Frau Dr.",
    "Herr Prof." => "Herr Prof.",
    "Frau Prof." => "Frau Prof.",
    "Herr Prof. Dr." => "Herr Prof. Dr.",
    "Frau Prof. Dr." => "Frau Prof. Dr."
);
?>
<style>
.SIO-logo {
  position: relative;
    margin-top: 0px;
    width: 313px;
    height: 80px;
    background: url(<?php echo plugins_url( 'images/selectionsio.png', __FILE__ ); ?>) center top/313px 63px no-repeat;
}
.SIO-version {
    position: absolute;
    width: 100%;
    bottom: 0;
    text-align: center;
    color: #72777c;
    line-height: 1em;
}
#textarea, #textarea2 {
    width: 400px;
}
</style>

<script>
		jQuery(document).ready(function($){
      <?php if($outputformdata != "") { ?>
        loadDataFromDB();
      <?php } ?>
function loadDataFromDB() {
  $('#container').html($("#fildend").val());
}


      var laender = <?php echo json_encode($laender); ?>;
      var anredearray = <?php echo json_encode($anredearray); ?>;
      var europeanCountries = <?php echo json_encode($europeanCountries); ?>;
      var options_de = "";
      var options_eu = "";
      var options_anrede = "";
      var options = "";
			// Liste der Formularfelder, die der Benutzer auswählen kann
			var container = [
				{ id: 'self_email', type: 'email', label: 'Email*', required: ' required' },
				{ id: 'password', type: 'password', label: 'Passwort*', required: ' required' },
				{ id: 'password_re', type: 'password', label: 'Passwort wiederholen*', required: ' required' },
				{ id: 'newsletter', type: 'checkbox', label: 'Newsletter empfangen?', required: '' },
				{ id: 'self_anrede', type: 'text', label: 'Anrede', required: '' },
				{ id: 'self_firstname', type: 'text', label: 'Vorname*', required: 'required' },
				{ id: 'self_lastname', type: 'text', label: 'Nachname*', required: 'required' },
				{ id: 'self_department', type: 'text', label: 'Firma / Verein', required: '' },
				{ id: 'self_street', type: 'text', label: 'Strasse*', required: 'required' },
				{ id: 'self_house_number', type: 'text', label: 'Hausnummer*', required: 'required' },
				{ id: 'self_zip', type: 'text', label: 'PLZ*', required: 'required' },
				{ id: 'self_city', type: 'text', label: 'Ort*', required: 'required' },
				{ id: 'self_country', type: 'select', label: 'Land*', required: 'required' },
				{ id: 'self_country_de', type: 'select', label: 'Land*', required: 'required' },
				{ id: 'self_country_eu', type: 'select', label: 'Land*', required: 'required' },
				{ id: 'self_country_w', type: 'select', label: 'Land*', required: 'required' },
				{ id: 'self_phone', type: 'text', label: 'Telefon', required: '' },
				{ id: 'self_phone_alternative', type: 'text', label: 'Fax', required: '' },
				{ id: 'ustid', type: 'text', label: 'UStID', required: '' },
				{ id: 'trennlinie', type: 'trennlinie', label: 'trennlinie' },
				{ id: 'button', type: 'submit', label: 'Daten Senden' }
			];

			// Felder zur Liste hinzufügen
      let trenncounter = 0;
			$('#addField').on('click', function() {
				var fieldId = $('#addFieldSelect').val();
				var fieldLabel = $('#addFieldSelect option:selected').text();
				var field = container.find(function(f) { return f.id == fieldId; });
				if (field != null) {
					// Feld hinzufügen
          console.log(fieldLabel);
          for (var id in anredearray) {

          }
          if(field.type == "trennlinie"){
            $('#container').append('<hr class="trennlinie" id="' + field.id + "_" + trenncounter + '" />\n');
            trenncounter++;
           } else if(fieldLabel == "Anrede") {
            options_anrede += '<div class="box" id="' + field.id + '"><label for="' + field.id + '">' + field.label + '</label><select id="self_anrede" name="self_anrede">\n';
            for (var id in anredearray) {
              options_anrede += '<option value="' + id + '">' + anredearray[id] + '</option>\n';
          }
          options_anrede += '</select></div>\n';
          console.log(options_anrede);
          $('#container').append(options_anrede);
          options_anrede = "";
          }  else if(fieldLabel == "Land nur Deutschland") {
            console.log("HIER");
            options_de = '<div class="box" id="' + field.id + '"><label for="' + field.id + '">' + field.label + '</label><select id="self_country" name="self_country"' + field.required +'>\n' + 
                          '<option value="DE">Deutschland</option>\n' + 
                          '</select></div>\n';
          console.log(options_de);
          $('#container').append(options_de);
          options_de = "";
          } else if(fieldLabel == "Land EU") {
            options_eu += '<div class="box" id="' + field.id + '"><label for="' + field.id + '">' + field.label + '</label><select id="self_country" name="self_country"' + field.required +'>\n';
            for (var code in laender) {
            if (europeanCountries.includes(code)) {
              options_eu += '<option value="' + code + '">' + laender[code] + '</option>\n';
            }
          }
          options_eu += '</select></div>\n';
          console.log(options_eu);
          $('#container').append(options_eu);
          options_eu = "";
          } else if(fieldLabel == "Land Weltweit") { 
            options += '<div class="box" id="' + field.id + '"><label for="' + field.id + '">' + field.label + '</label><select id="self_country" name="self_country"' + field.required +'>\n';
            for (var code in laender) {
              options += '<option value="' + code + '">' + laender[code] + '</option>\n';
          }
              options += '</select></div>\n';
          console.log(options);
          $('#container').append(options);
          options = "";
          } else if(fieldLabel == "Button") { 
          $('#container').append('<div class="box" id="' + field.id + '"><input type="' + field.type + '" id="' + field.id + '" name="' + field.id + '" value="' + field.label + '"></div>\n');
          options = "";
          } else {
					$('#container').append('<div class="box" id="' + field.id + '"><label for="' + field.id + '">' + fieldLabel + '</label><input type="' + field.type + '" id="' + field.id + '" name="' + field.id + '"' + field.required +'></div>\n');
          }
				}
        $("#fildend").val($('#container').html());
			});
// Felder von der Liste entfernen
      $('#removeField').on('click', function() {
        console.log("remove");
				var fieldId = $('#addFieldSelect').val();
				var fieldLabel = $('#addFieldSelect option:selected').text();
				var field = container.find(function(f) { return f.id == fieldId; });
        console.log(field);
				if (field != null) {
          console.log(field.type);
					// Feld hinzufügen
          if(field.type == "trennlinie"){
            delcounter = trenncounter-1;
            console.log("#" + field.type + "_" + delcounter);
					$("#" + field.id + "_" + delcounter).remove();
          trenncounter--;
          } else if(field.id == "self_country_eu") { 
            $("#" + field.id).remove();
            options_eu = "";
           } else if(field.id == "self_country_w") { 
            $("#" + field.id).remove();
            options = "";
           } else {
            $("#" + field.id).remove();
          }
				}
        $("#fildend").val($('#container').html());
			});


      $('#addZusatzField').on('click', function() {
            console.log("KLICK");
            $('#container').html($("#fildend").val());
            $('#formcssdata').html($("#fildcssend").val());
      });
		});
	</script>
<?php
if(isset($_GET["tab"]) AND $_GET["tab"] == "loginform") {
  $container = array(
    array(
        'id' => 'self_email',
        'label' => 'Email*',
    ),
    array(
        'id' => 'password',
        'label' => 'Passwort*',
    ),
    array(
      'id' => 'button',
      'label' => 'Button',
    ));
} else if(isset($_GET["tab"]) AND $_GET["tab"] == "pwlostform") {
  $container = array(
    array(
        'id' => 'self_email',
        'label' => 'Email*',
    ),
    array(
      'id' => 'button',
      'label' => 'Button',
    ));
} else {
$container = array(
  array(
      'id' => 'self_email',
      'label' => 'Email*',
  ),
  array(
      'id' => 'password',
      'label' => 'Passwort*',
  ),
  array(
      'id' => 'password_re',
      'label' => 'Passwort wiederholen*',
  ),
  array(
      'id' => 'newsletter',
      'label' => 'Newsletter empfangen?',
  ),
  array(
    'id' => 'self_anrede',
    'label' => 'Anrede',
),
array(
  'id' => 'self_firstname',
  'label' => 'Vorname*',
),
array(
  'id' => 'self_lastname',
  'label' => 'Nachname*',
),
array(
  'id' => 'self_department',
  'label' => 'Firma / Verein',
),
array(
  'id' => 'self_street',
  'label' => 'Strasse*',
),
array(
  'id' => 'self_house_number',
  'label' => 'Hausnummer*',
),
array(
  'id' => 'self_city',
  'label' => 'Stadt*',
),
array(
  'id' => 'self_zip',
  'label' => 'PLZ*',
),
array(
  'id' => 'self_country_de',
  'label' => 'Land nur Deutschland',
),
array(
  'id' => 'self_country_eu',
  'label' => 'Land EU',
),
array(
  'id' => 'self_country_w',
  'label' => 'Land Weltweit',
),
array(
  'id' => 'self_phone',
  'label' => 'Telefon',
),
array(
  'id' => 'self_phone_alternative',
  'label' => 'Fax',
),
array(
  'id' => 'ustid',
  'label' => 'UStID',
),
  array(
    'id' => 'trennlinie',
    'label' => 'Trennlinie',
),
array(
  'id' => 'button',
  'label' => 'Button',
)
  // weitere Felder hier...
);
}
?>
<script>
// Get the container element
const container = document.getElementById('container');

// Get all the boxes inside the container
const boxes = container.querySelectorAll('.box');

// Add a dragstart event listener to each box
boxes.forEach(box => {
  box.addEventListener('dragstart', e => {
    // Get the current index of the box
    const currentIndex = Array.from(boxes).indexOf(box);

    // Set the data of the drag event to the current index
    e.dataTransfer.setData('text/plain', currentIndex);
  });
});

// Add a dragover event listener to the container
container.addEventListener('dragover', e => {
  e.preventDefault();
});

// Add a drop event listener to the container
container.addEventListener('drop', e => {
  e.preventDefault();

  // Get the index of the box being dragged
  const currentIndex = parseInt(e.dataTransfer.getData('text/plain'));

  // Get the index of the box where the dragged box is dropped
  const newIndex = Array.from(boxes).indexOf(e.target);

  // Move the dragged box to the new position in the DOM
  if (currentIndex < newIndex) {
    container.insertBefore(boxes[currentIndex], boxes[newIndex].nextSibling);
  } else {
    container.insertBefore(boxes[currentIndex], boxes[newIndex]);
  }
});
</script>

<style>
		#container {
			border: 1px solid #ddd;
			padding: 10px;
			margin-bottom: 20px;
      background-color: #fff;
      width: 50%;
      display: flex;
  flex-direction: column;
  position: relative;
		}
		.box {
      width: 100%;
			margin-bottom: 10px;
      display: absolute;
		}
		.removeField {
			color: red;
			cursor: pointer;
		}
    #fildend, #fildcssend {
    width: 50%;
    height: 300px;
    }
    </style>
<style id="formcssdata">
<?php if($outputcssdata != "") { echo $outputcssdata; } else { ?>
/* Formular */
.box {
margin-bottom: 0px;
}
.box label {
display: block;
margin-bottom: 5px;
}
.box input[type=email],
.box input[type=password],
.box #password_re,
.box input[type=text] {
font-size: 22px;
width: 100%;
padding: 5px 10px;
border: none;
border-radius: 5px;
background-color: #e8f0fe;
}
.box input[type=email]:focus,
.box input[type=password]:focus,
.box #password_re:focus,
.box input[type=text]:focus {
outline: none;
background-color: #e6e6e6;
}
.box input[type=checkbox] {
margin-right: 10px;
}
.box select {
width: 100%;
padding: 0px 10px;
border: none;
border-radius: 5px;
background-color: #e8f0fe;
}
.box select:focus {
outline: none;
background-color: #e6e6e6;
}
.trennlinie {
border: none;
width: 100%;
height: 2px;
margin-top: 30px;
margin-bottom: 30px;
background-color: #0000ff;
}

input[type=checkbox] {
  appearance: none;
  -webkit-appearance: none;
  -moz-appearance: none;
  width: 20px;
  height: 20px;
  border: 1px solid #ccc;
  border-radius: 3px;
  transition: all 0.3s ease;
  outline: none;
  cursor: pointer;
}

input[type=checkbox]:checked {
  background-color: #3498db;
  border-color: #3498db;
}

input[type=checkbox]:after {
  font-size: 16px;
  color: #fff;
  position: absolute;
  top: 2px;
  left: 6px;
  transition: all 0.3s ease;
  opacity: 0;
}

input[type=checkbox]:checked:after {
  opacity: 1;
}

input#button {
    margin-top: 20px;
    width: 120px;
    background-color: #8ac03c;
    color: #fff;
    padding: 10px 10px;
    border: none;
    border-radius: 5px;
    font-size: 16px;
    cursor: pointer;
    float:right;
}
<?php } ?>
</style>
<div class="wrap">
    <a target="_blank" href="https://Selectionsio.de/"><div class="SIO-logo">
			<div class="SIO-version">Selectionsio Produkt-Konfigurator v.1.0.0</div>
		</div></a>
        <h1>Selectionsio Formulareditor</h1>
        <h2>Registerformular:</h2>
<h2 class="nav-tab-wrapper">
    <a href="?page=Selectionsioapi_formeditor_page&tab=general" class="nav-tab <?php echo $active_tab == 'general' ? 'nav-tab-active' : ''; ?>">Allgemein</a>
    <a href="?page=Selectionsioapi_formeditor_page&tab=loginform" class="nav-tab <?php echo $active_tab == 'loginform' ? 'nav-tab-active' : ''; ?>">Loginformular</a>
    <a href="?page=Selectionsioapi_formeditor_page&tab=pwlostform" class="nav-tab <?php echo $active_tab == 'pwlostform' ? 'nav-tab-active' : ''; ?>">Passwort vergessen Formular</a>
    <a href="?page=Selectionsioapi_formeditor_page&tab=deliveryandinvoiceform" class="nav-tab <?php echo $active_tab == 'deliveryandinvoiceform' ? 'nav-tab-active' : ''; ?>">Liefer & Rechnungsadresse-Formular</a>
    <a href="?page=Selectionsioapi_formeditor_page&tab=registerform" class="nav-tab <?php echo $active_tab == 'registerform' ? 'nav-tab-active' : ''; ?>">Registrierformular</a>
</h2>
<br /><br />
<?php
switch ( $active_tab ) {
    case 'loginform':
    case 'pwlostform':
    case 'deliveryandinvoiceform':
    case 'registerform':
?>
        <label for="addFieldSelect">Feld hinzufügen:</label>
	<select id="addFieldSelect">
		<?php foreach ($container as $field): ?>
			<option value="<?php echo $field['id']; ?>"><?php echo $field['label']; ?></option>
		<?php endforeach; ?>
	</select>
	<button id="addField">Feld hinzufügen</button> <button id="removeField">Feld entfernen</button>
<!-- Ausgewählte Formularfelder -->
<div id="container"></div>

<!-- Optionen für die ausgewählten Felder -->

<div id="fieldOptions"></div>
<form action="" method="post">
<input type="hidden" name="tab" value="<?php echo $_GET["tab"]; ?>" id="tab">
<label>Formular:</label><br />
<textarea name="formdatas" id="fildend"><?php if($outputformdata != "") { echo $outputformdata; } ?></textarea><br />
<label>CSS vom Formular:</label><br />
<textarea name="cssdatas" id="fildcssend"><?php if($outputcssdata != "") { echo $outputcssdata; } else { ?>/* Formular */
div#container {
    width: 50%;
}
/* Formular */
.box {
margin-bottom: 0px;
}
.box label {
display: block;
margin-bottom: 5px;
}
.box input[type=email],
.box input[type=password],
.box #password_re,
.box input[type=text] {
font-size: 22px;
width: 100%;
padding: 5px 10px;
border: none;
border-radius: 5px;
background-color: #e8f0fe;
}
.box input[type=email]:focus,
.box input[type=password]:focus,
.box #password_re:focus,
.box input[type=text]:focus {
outline: none;
background-color: #e6e6e6;
}
.box input[type=checkbox] {
margin-right: 10px;
}
.box select {
width: 100%;
padding: 0px 10px;
border: none;
border-radius: 5px;
background-color: #e8f0fe;
}
.box select:focus {
outline: none;
background-color: #e6e6e6;
}
.trennlinie {
border: none;
height: 2px;
margin-top: 30px;
margin-bottom: 30px;
background-color: #0000ff;
}

input[type=checkbox] {
  appearance: none;
  -webkit-appearance: none;
  -moz-appearance: none;
  width: 20px;
  height: 20px;
  border: 1px solid #ccc;
  border-radius: 3px;
  transition: all 0.3s ease;
  outline: none;
  cursor: pointer;
}

input[type=checkbox]:checked {
  background-color: #3498db;
  border-color: #3498db;
}

input[type=checkbox]:after {
  content: "\2714";
  font-size: 16px;
  color: #fff;
  position: absolute;
  top: 2px;
  left: 6px;
  transition: all 0.3s ease;
  opacity: 0;
}

input[type=checkbox]:checked:after {
  opacity: 1;
}

input#button {
    margin-top: 20px;
    width: 120px;
    background-color: #8ac03c;
    color: #fff;
    padding: 10px 10px;
    border: none;
    border-radius: 5px;
    font-size: 16px;
    cursor: pointer;
    float:right;
}<?php } ?></textarea><br />
<button id="addSendData" style="background-color: #8ac03c;
    color: #fff;
    padding: 10px 20px;
    border: none;
    border-radius: 5px;
    font-size: 16px;
    cursor: pointer;float:left;">Formular speichern</button>
</form>
<button id="addZusatzField" style="background-color: #007bff;
    margin-left: 20px;
    color: #fff;
    padding: 10px 20px;
    border: none;
    border-radius: 5px;
    font-size: 16px;
    cursor: pointer;float:left;">Formularvorschau aktualisieren</button><br /><br />
<?php
break;
default:
    // Laden Sie den Inhalt für den Standard-Tab (in diesem Beispiel "Allgemein")
    ?>
Bitte Formulare über die Tabs erzeugen!
    <?php
    break;
} } ?>