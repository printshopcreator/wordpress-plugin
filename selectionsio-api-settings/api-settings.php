<?php
/*
* Plugin Name:selectionsio API-Connect
* Plugin URI: https://selectionsio.de/api-einstellungen
* Description: Mit dieser App können Sie Selectionsio Kalkulationen und Produktkonfiguratoren direkt in Ihre WordPress-Website einbinden. With this app you can embed Selectionsio calculations and product configurators directly into your WordPress website.
* Version: 1.0.0
* Author: Selectionsio GmbH
* Author URI: https://selectionsio.de
* License: GPL2
*/
include_once 'customer.php';
include_once 'userlogin.php';
include_once 'userregister.php';
include_once 'usersetting.php';
include_once 'userorders.php';
include_once 'formeditor.php';
include_once 'order.php';
include_once 'payment.php';
include_once 'shipping.php';
include_once 'shortcodes.php';
include_once 'shortcodes_wk.php';
include_once 'shortcodes_checkout.php';
if(isset($_POST['sio_product_uuid']) && isset($_POST['sio_product_image_url'])) {
function handle_form_submission() {
        global $wpdb;
        
        // Prüfen Sie, ob das Formular gesendet wurde
        if (isset($_POST['submit'])) {
          // Holen Sie sich die Werte des Formulars
          $produkt_id = $_POST['sio_product_uuid'];
          $produkt_image_url = $_POST['sio_product_image_url'];
          $style = $_POST['sio_style'];
      
          // Überprüfen Sie, ob der Eintrag bereits in der Datenbank vorhanden ist
          $existing_entry = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}Selectionsioapi_imageplugin WHERE `produkt_id` = '".$produkt_id."'");
          $existing_entry_css = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}Selectionsioapi_cssplugin");
          //var_dump($existing_entry);
          if ($existing_entry) {
            // Aktualisieren Sie den Eintrag in der Datenbank
            $wpdb->update("{$wpdb->prefix}Selectionsioapi_imageplugin", array(
              'produkt_image_url' => $produkt_image_url
            ), array(
              'produkt_id' => $produkt_id
            ));
          } else {
            // Fügen Sie einen neuen Eintrag in die Datenbank hinzu
            $wpdb->insert("{$wpdb->prefix}Selectionsioapi_imageplugin", array(
              'produkt_id' => $produkt_id,
              'produkt_image_url' => $produkt_image_url
            ));
          }
          if($existing_entry_css AND $style != "") {
            // Aktualisieren Sie den Eintrag in der Datenbank
            $wpdb->update("{$wpdb->prefix}Selectionsioapi_cssplugin", array(
                'style' => $style
              ), array(
                'id' => 1
              ));
          } else {
            $wpdb->insert("{$wpdb->prefix}Selectionsioapi_cssplugin", array(
                'style' => $style
              ));
          }
        }
      }
      add_action('admin_init', 'handle_form_submission');
    }
// Erstelle Seite für API Einstellungen
function Selectionsioapi_settings_page() {
    global $wpdb;
    $table_name = $wpdb->prefix . "Selectionsioapi_imageplugin where `produkt_id` = '".get_option('sio_product_uuid')."'";
    $results = $wpdb->get_results( "SELECT * FROM $table_name" );
    
    $table_name = $wpdb->prefix . "Selectionsioapi_cssplugin";
    $results2 = $wpdb->get_results( "SELECT * FROM $table_name" );
    wp_enqueue_media();
    ?>
<style>
.SIO-logo {
    position: relative;
    width: 313px;
    height: 80px;
    background: url(<?php echo plugins_url( 'images/selectionsio.png', __FILE__ ); ?>) center top/313px 63px no-repeat;
}
.SIO-version {
    position: absolute;
    width: 100%;
    bottom: 0;
    text-align: center;
    color: #72777c;
    line-height: 1em;
}
#textarea, #textarea2, #textarea3, #textarea4, #textarea5 {
    width: 400px;
}
#sio_style {
    width: 400px;
    height: 300px;
}
</style>

<div class="wrap">
    <a target="_blank" href="https://Selectionsio.de/"><div class="SIO-logo">
			<div class="SIO-version">Selectionsio Produkt-Konfigurator v.1.0.0</div>
		</div></a>
        <h1>Selectionsio Produkt-Konfigurator</h1>
        <h2>Es wird immer das zuletzt konfigurierte Produkt geladen!</h2>
        <form method="post" action="options.php" id="thisform">
        <input type="hidden" id="sio_product_image_url" name="sio_product_image_url" value="<?php echo $results[0]->produkt_image_url; ?>" />
            <?php
                settings_fields( 'Selectionsioapi-settings-group' );
                do_settings_sections( 'Selectionsioapi-settings-group' );
            ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">SIO URL</th>
                    <td><input class="regular-text ltr" type="text" id="sio_api_link" name="sio_api_link" value="<?php echo esc_attr( get_option('sio_api_link') ); ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Shop UUID</th>
                    <td><input class="regular-text ltr" type="text" id="sio_shop_uuid" name="sio_shop_uuid" value="<?php echo esc_attr( get_option('sio_shop_uuid') ); ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">API Token</th>
                    <td><input class="regular-text ltr" type="text" name="sio_api_token" value="<?php echo esc_attr( get_option('sio_api_token') ); ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">PSC Benutzername</th>
                    <td><input class="regular-text ltr" type="text" name="sio_api_username" value="<?php echo esc_attr( get_option('sio_api_username') ); ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">PSC Passwort</th>
                    <td><input class="regular-text ltr" type="password" name="sio_api_password" value="<?php echo esc_attr( get_option('sio_api_password') ); ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Preis Ausgabe:</th>
                    <td>
                    <select name="sio_api_price" id="sio_api_price">
                            <option value="1" <?php if(get_option('sio_api_price') == '1') echo 'selected'; ?>>Preis anzeigen</option>
                            <option value="2" <?php if(get_option('sio_api_price') == '2') echo 'selected'; ?>>Wert anzeigen</option>
                            <option value="0" <?php if(get_option('sio_api_price') == '0') echo 'selected'; ?>>Preis & Wert nicht anzeigen</option>
                    </select>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Maßeinheit</th>
                        <td><input class="regular-text ltr" type="text" id="sio_shop_einheit" name="sio_shop_einheit" value="<?php echo esc_attr( get_option('sio_shop_einheit') ); ?>" /></td>
                    </tr>
                    <tr valign="top">
                    <th scope="row">Bestellenbutton:</th>
                    <td>
                    <select name="sio_api_salebutton" id="sio_api_salebutton">
                            <option value="1" <?php if(get_option('sio_api_salebutton') == '1') echo 'selected'; ?>>Bestellenbutton anzeigen</option>
                            <option value="0" <?php if(get_option('sio_api_salebutton') == '0') echo 'selected'; ?>>Bestellenbutton nicht anzeigen</option>
                    </select>
                    </tr>
                    <tr valign="top">
                    <th scope="row">PSC Produktbeschreibung:</th>
                    <td>
                    <select name="sio_api_descriptionbutton" id="sio_api_descriptionbutton">
                            <option value="1" <?php if(get_option('sio_api_descriptionbutton') == '1') echo 'selected'; ?>>PSC Produktbeschreibung über Kalkulation anzeigen</option>
                            <option value="2" <?php if(get_option('sio_api_descriptionbutton') == '2') echo 'selected'; ?>>PSC Produktbeschreibung unter Kalkulation anzeigen</option>
                            <option value="0" <?php if(get_option('sio_api_descriptionbutton') == '0') echo 'selected'; ?>>PSC Produktbeschreibung nicht anzeigen</option>
                    </select>
                    </tr>
                 <tr valign="top">
                    <th scope="row">Produktgruppe</th>
                    <td>
                        <select name="sio_api_prgroup" id="sio_api_prgroup"></select>
                    </td>
                 </tr>
                 <tr valign="top">
                    <th scope="row">Produkte</th>
                    <td>
                        <select name="sio_products" id="sio_products"></select>
                    </td>
                 </tr>
                 <tr valign="top" id="sio_product_uuid_field" style="display: none;">
                <th scope="row">Product UUID</th>
                <td><input class="regular-text ltr" type="text" name="sio_product_uuid" id="sio_product_uuid" value="<?php echo esc_attr( get_option('sio_product_uuid') ); ?>" /></td>
            </tr>
            <tr valign="top">
                    <th scope="row">Globales-CSS:</th>
                    <td>
                    <textarea id="sio_style" name="sio_style" id="sio_style"><?php echo $results2[0]->style; ?></textarea><br />
                    </td>
                 </tr>
        </table>
        <button class="button button-primary" id="SelectionsioopenMediaPics" onclick="event.preventDefault();">Medien</button><br /><br />
        <?php submit_button( 'Bitte jedes Produkt Speichern!', 'primary', 'submit', false ); ?>
    </form><br /><br />
    <div id="your-image-container"><img src="<?php echo $results[0]->produkt_image_url; ?>"></div>
</div>
<script>
jQuery(document).ready(function($){
  var image_frame;
  var productimageurl = document.getElementById('sio_product_image_url');
  $('#SelectionsioopenMediaPics').click(function(e){
    e.preventDefault();
    // Check if the `image_frame` has been defined
    if( typeof image_frame !== 'undefined' ) {
      image_frame.open();
      return;
    }
    // Define the media frame
    image_frame = wp.media({
      title: 'Select Image',
      multiple : false,
      library : {
         type : 'image',
      }
    });
    image_frame.on('select',function(){
      var selection =  image_frame.state().get('selection');
      selection.each(function(attachment){
        var url = attachment.attributes.url;
        $('#your-image-container').html('<img src="'+url+'"/>');
        productimageurl.value = url;
      });
    });
    image_frame.open();
  });
});



    var calcHasError = false;
    var $ = jQuery.noConflict();
    var url = document.getElementById('sio_api_link');
    var shopUUID = document.getElementById('sio_shop_uuid');
    var apiPrgroup = document.getElementById('sio_api_prgroup');
    var productUuidField = document.getElementById('sio_product_uuid_field');
    var apiPriceSelect = document.getElementById('sio_api_price');
    var apiSaleSelect = document.getElementById('sio_api_salebutton');
    var apiDescriptionSelect = document.getElementById('sio_api_descriptionbutton');
    var products = document.getElementById('sio_products');
    var productUuid = document.getElementById('sio_product_uuid');
    var measure = document.getElementById('sio_shop_einheit');
    var thisform = document.getElementById('thisform');
    var selectedgroup = "<?php echo esc_attr( get_option('sio_api_prgroup') ); ?>";
    var selectedproduct = "<?php echo esc_attr( get_option('sio_products') ); ?>";
    var textareaField = document.getElementById('textarea');

function Selectionsio_loadProducts() {
		$('#sio_products').empty();

		$.ajax({
			url: url.value +  "/apps/api/product/getallbyproductgroup/" + $('#sio_api_prgroup').val(),
			contentType: "application/json",
			method: 'GET',
			success: function(result) {
				$.each(result.data, function(index, value) {
					Selectionsio_appendProduct(value);
				});

				//loadCalc();
			}
		})
	}
    function Selectionsio_appendProduct(data, depth = '') {
        if(data.uuid == selectedproduct) {
            var selectfildpr = "selected";
        }
		$('#sio_products').append('<option value="' + data.uuid + '" ' + selectfildpr + '>' + depth + data.title + '</option>');
        setSelectionsioProductUuid();
	}
function setSelectionsioProductUuid() {
    productUuid.value = products.value;

const shortcodeOptions = {
    calcid: products.value,
    price: apiPriceSelect.value === '1' ? 1 :
       apiPriceSelect.value === '2' ? 2 :
       apiPriceSelect.value === '0' ? 0 : null,
    salebtn: apiSaleSelect.value === '1' ? 1 : 0,
    description: apiDescriptionSelect.value === '1' ? 1 :
    apiDescriptionSelect.value === '2' ? 2 :
    apiDescriptionSelect.value === '0' ? 0 : null,
    measure: measure.value || ''
};

let shortcodeString = '[Selectionsioproduct_calculator';

for (const key in shortcodeOptions) {
    if (shortcodeOptions[key]) {
        shortcodeString += ` ${key}="${shortcodeOptions[key]}"`;
    }
}
shortcodeString += ']';

productUuidField.style.display = 'table-row';
shortcode.style.display = 'block';
shortcode.innerHTML = `Bitte mit dem Folgenden Shortcode einbinden: ${shortcodeString}`;

document.getElementById('textarea').value = `${shortcodeString}`;
}
function Selectionsio_loadProductGroups() {
		$.ajax({
			url: url.value +  "/apps/api/productgroup/gettree/" + shopUUID.value,
			contentType: "application/json",
			method: 'GET',
			success: function(result) {
				$.each(result.data, function(index, value) {
					Selectionsio_appendProductGroup(value);
				});
			}
		})
	}

	function Selectionsio_appendProductGroup(data, depth = '') {
        if(data.uuid == selectedgroup) {
            var selectfildgr = " selected";
            Selectionsio_loadProducts();
        } else {
            var selectfildgr = "";
        }
		$('#sio_api_prgroup').append('<option value="' + data.uuid + '"' + selectfildgr + '>' + depth + data.title + '</option>');
        //
		$.each(data.children, function(index, value) {
			Selectionsio_appendProductGroup(value, depth + '>');
		})
        if(data.uuid == selectedgroup) {
            Selectionsio_loadProducts();
        }
	}
    document.addEventListener('DOMContentLoaded', function() {
document.querySelector("button").onclick = function(){
  document.querySelector("textarea").select();
  document.execCommand('copy');
};
Selectionsio_loadProductGroups();
        if(productUuid.value != "") {
            productUuidField.style.display = 'table-row';
            shortcode.style.display = 'block';
            const shortcodeOptions = {
    calcid: products.value,
    price: apiPriceSelect.value === '1' ? 1 :
       apiPriceSelect.value === '2' ? 2 :
       apiPriceSelect.value === '0' ? 0 : null,
    salebtn: apiSaleSelect.value === '1' ? 1 : 0,
    description: apiDescriptionSelect.value === '1' ? 1 :
    apiDescriptionSelect.value === '2' ? 2 :
    apiDescriptionSelect.value === '0' ? 0 : null,
    measure: measure.value || ''
};

let shortcodeString = '[Selectionsioproduct_calculator';

for (const key in shortcodeOptions) {
    if (shortcodeOptions[key]) {
        shortcodeString += ` ${key}="${shortcodeOptions[key]}"`;
    }
}
shortcodeString += ']';

productUuidField.style.display = 'table-row';
shortcode.style.display = 'block';
shortcode.innerHTML = `Bitte mit dem Folgenden Shortcode einbinden: ${shortcodeString}`;

document.getElementById('textarea').value = `${shortcodeString}`;
        }
apiPrgroup.addEventListener('change', function() {
Selectionsio_loadProducts();
    });
products.addEventListener('change', function() {
setSelectionsioProductUuid();
    });
thisform.addEventListener('change', function() {
    const shortcodeOptions = {
    calcid: products.value,
    price: apiPriceSelect.value === '1' ? 1 :
       apiPriceSelect.value === '2' ? 2 :
       apiPriceSelect.value === '0' ? 0 : null,
    salebtn: apiSaleSelect.value === '1' ? 1 : 0,
    description: apiDescriptionSelect.value === '1' ? 1 :
    apiDescriptionSelect.value === '2' ? 2 :
    apiDescriptionSelect.value === '0' ? 0 : null,
    measure: measure.value || ''
};
let shortcodeString = '[Selectionsioproduct_calculator';

for (const key in shortcodeOptions) {
    if (shortcodeOptions[key]) {
        shortcodeString += ` ${key}="${shortcodeOptions[key]}"`;
    }
}
shortcodeString += ']';

productUuidField.style.display = 'table-row';
shortcode.style.display = 'block';
shortcode.innerHTML = `Bitte mit dem Folgenden Shortcode einbinden: ${shortcodeString}`;

document.getElementById('textarea').value = `${shortcodeString}`;
});
measure.addEventListener('change', function() {
if(apiSaleSelect.value == '1' && apiPriceSelect.value == "1" && apiDescriptionSelect.value == "1" && measure.value != "") {
}
    });
    });
function Selectionsio_copyToClipBoard() {

var content = document.getElementById('textarea');

content.select();
document.execCommand('copy');

msg.innerHTML = "Product Shortcode in die Zwischenablage kopiert.";
}
function Selectionsio_copyToClipBoardID(id, type) {

var content = document.getElementById('textarea' + id);

content.select();
document.execCommand('copy');
var msg = document.getElementById('msg' + id);
msg.innerHTML = type + " Shortcode in die Zwischenablage kopiert.";
}
</script>
<div id="shortcode" style="display: none;"></div>
<br/>
<div id="msg"></div>
<textarea id="textarea"></textarea><br />
<button onclick="Selectionsio_copyToClipBoard()">Product Shortcode in Zwischenablage kopieren</button>

<div id="msg2"></div>
<textarea id="textarea2">[Selectionsio_wk divID="ID vom div, der vor den Warenkob liegt hier eintragen!"]</textarea><br />
<button onclick="Selectionsio_copyToClipBoardID(2, 'Warenkorb')">Warenkorb Shortcode in Zwischenablage kopieren</button>

<div id="msg3"></div>
<textarea id="textarea3">[Selectionsio_checkout divID="ID vom div, der vor der Kasse liegt hier eintragen!"]</textarea><br />
<button onclick="Selectionsio_copyToClipBoardID(3, 'Kasse')">Kasse Shortcode in Zwischenablage kopieren</button>

<div id="msg4"></div>
<textarea id="textarea4">[Selectionsio_my_login_form]</textarea><br />
<button onclick="Selectionsio_copyToClipBoardID(4, 'Login')">Login Shortcode in Zwischenablage kopieren</button>

<div id="msg5"></div>
<textarea id="textarea5">[Selectionsio_my_register_form]</textarea><br />
<button onclick="Selectionsio_copyToClipBoardID(5, 'Register')">Register Shortcode in Zwischenablage kopieren</button>
<?php
if(get_option('sio_api_username') != "" and get_option('sio_api_password') != "") {
$urlAPI = esc_attr( get_option('sio_api_link') ) . "/apps/api/contact/login";
//$allAccountsID->getApiToken();
$debug = 1;

$data = array(
    'username' => esc_attr( get_option('sio_api_username')),
    'password' => esc_attr( get_option('sio_api_password'))
  );
  $options = array(
    'http' => array(
        'method' => 'POST',
        'content' => json_encode($data),
        'header' => "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context = stream_context_create($options);
$result = file_get_contents($urlAPI, false, $context);
$response = json_decode($result);
update_option('sio_api_token', $response->token);
//echo "<br /><br />JWT Token: " .  esc_attr( get_option('sio_api_token'));
}
}
// Registriere Einstellungen
function Selectionsioapi_settings_init() {
    register_setting( 'Selectionsioapi-settings-group', 'sio_api_link' );
    register_setting( 'Selectionsioapi-settings-group', 'sio_shop_uuid' );
    register_setting( 'Selectionsioapi-settings-group', 'sio_api_token' );
    register_setting( 'Selectionsioapi-settings-group', 'sio_api_username' );
    register_setting( 'Selectionsioapi-settings-group', 'sio_api_password' );
    register_setting( 'Selectionsioapi-settings-group', 'sio_api_price' );
    register_setting( 'Selectionsioapi-settings-group', 'sio_shop_einheit' );
    register_setting( 'Selectionsioapi-settings-group', 'sio_api_salebutton' );
    register_setting( 'Selectionsioapi-settings-group', 'sio_api_descriptionbutton' );
    register_setting( 'Selectionsioapi-settings-group', 'sio_api_prgroup' );
    register_setting( 'Selectionsioapi-settings-group', 'sio_product_uuid' );
    register_setting( 'Selectionsioapi-settings-group', 'sio_products' );
    register_setting( 'Selectionsioapi-settings-group', 'sio_api_token' );
}
add_action( 'admin_init', 'Selectionsioapi_settings_init' );

function Selectionsioapi_settings_link($links) { 
    $settings_link = '<a href="admin.php?page=Selectionsioapi-settings">Einstellungen</a>'; 
    array_unshift($links, $settings_link); 
    return $links; 
  }
  add_filter("plugin_action_links_" . plugin_basename(__FILE__), 'Selectionsioapi_settings_link');


  function Selectionsioapi_imageplugin_install() {
    global $wpdb;
    $table_name = $wpdb->prefix . "Selectionsioapi_imageplugin";
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      produkt_id varchar(100) NOT NULL,
      produkt_image_url varchar(255) NOT NULL,
      PRIMARY KEY  (id)
    ) $charset_collate;";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
  }
  register_activation_hook( __FILE__, 'Selectionsioapi_imageplugin_install' );

  function Selectionsioapi_cssplugin_install() {
    global $wpdb;
    $table_name = $wpdb->prefix . "Selectionsioapi_cssplugin";
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      style text NOT NULL,
      PRIMARY KEY  (id)
    ) $charset_collate;";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
  }
  register_activation_hook( __FILE__, 'Selectionsioapi_cssplugin_install' );

  function Selectionsioapi_formplugin_install() {
    global $wpdb;
    $table_name = $wpdb->prefix . "Selectionsioapi_formplugin";
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      registerform text NOT NULL,
      deliveryandinvoiceform text NOT NULL,
      loginform text NOT NULL,
      pwlostform text NOT NULL,
      formcssdata text NOT NULL,
      PRIMARY KEY  (id)
    ) $charset_collate;";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    $existing_entry = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}Selectionsioapi_formplugin WHERE `id` = 1");
    if (!$existing_entry) {
    $wpdb->insert("{$wpdb->prefix}Selectionsioapi_formplugin", array(
        'registerform' => "firstdata",
        'deliveryandinvoiceform' => "firstdata",
        'loginform' => "firstdata",
        'pwlostform' => "firstdata",
        'formcssdata' => "firstdata"
      ));
    }
  }
  register_activation_hook( __FILE__, 'Selectionsioapi_formplugin_install' );

add_role( 'psc_role', 'PSC-Kunde', array(
    'read' => true,
    'edit_posts' => false,
    'delete_posts' => false,
) );
// Erstelle neuen Menüpunkt im Backend
function Selectionsioapi_settings_menu() {
    add_menu_page( 'S/IO API-Connect', 'S/IO API-Connect', 'manage_options', 'Selectionsioapi-settings', 'Selectionsioapi_settings_page', 'dashicons-rest-api' );
    add_submenu_page( 'Selectionsioapi-settings', 'Formulareditor', 'Formulareditor', 'manage_options', 'Selectionsioapi_formeditor_page', 'Selectionsioapi_formeditor_page' );
    add_submenu_page( 'Selectionsioapi-settings', 'Aufträge', 'Aufträge', 'manage_options', 'Selectionsioapi_order_page', 'Selectionsioapi_order_page' );
    add_submenu_page( 'Selectionsioapi-settings', 'Kunden', 'Kunden', 'manage_options', 'Selectionsioapi_customer_page', 'Selectionsioapi_customer_page' );
    add_submenu_page( 'Selectionsioapi-settings', 'Zahlarten', 'Zahlarten', 'manage_options', 'Selectionsioapi_payment_page', 'Selectionsioapi_payment_page' );
    add_submenu_page( 'Selectionsioapi-settings', 'Versandarten', 'Versandarten', 'manage_options', 'Selectionsioapi_shipping_page', 'Selectionsioapi_shipping_page' );
}
add_action( 'admin_menu', 'Selectionsioapi_settings_menu' );

  function selectionsio_admin_link($wp_admin_bar) {
    if ( current_user_can( 'administrator' ) ) {
    $wp_admin_bar->add_menu(array(
        'id' => 'selectionsio-submenu',
        'title' => 'S/IO API-Connect',
        'href'  => '/wp-admin/admin.php?page=Selectionsioapi-settings',
        'parent' => 'appearance',
        'meta' => array(
            'class' => 'selectionsio-submenu-class',
            'title' => 'S/IO API-Connect'
        ),
    ));
    $args = array(
        'id'    => 'selectionsio-subsubmenu-link0',
        'title' => 'Formulareditor',
        'href'  => '/wp-admin/admin.php?page=Selectionsioapi_formeditor_page',
        'meta'  => array(
            'class' => 'selectionsio-subsubmenu-link-class',
            'title' => 'Formulareditor'
        ),
        'parent' => 'selectionsio-submenu',
    );
    $wp_admin_bar->add_menu($args);
    $args = array(
        'id'    => 'selectionsio-subsubmenu-link-1',
        'title' => 'Allgemein',
        'href'  => '/wp-admin/admin.php?page=Selectionsioapi_formeditor_page',
        'meta'  => array(
            'class' => 'selectionsio-subsubmenu-link-class',
            'title' => 'Allgemein'
        ),
        'parent' => 'selectionsio-subsubmenu-link0',
    );
    $wp_admin_bar->add_menu($args);
    $args = array(
        'id'    => 'selectionsio-subsubmenu-link-2',
        'title' => 'Loginformular',
        'href'  => '/wp-admin/admin.php?page=Selectionsioapi_formeditor_page&tab=loginform',
        'meta'  => array(
            'class' => 'selectionsio-subsubmenu-link-class',
            'title' => 'Loginformular'
        ),
        'parent' => 'selectionsio-subsubmenu-link0',
    );
    $wp_admin_bar->add_menu($args);
    $args = array(
        'id'    => 'selectionsio-subsubmenu-link-3',
        'title' => 'Passwort vergessen Formular',
        'href'  => '/wp-admin/admin.php?page=Selectionsioapi_formeditor_page&tab=pwlostform',
        'meta'  => array(
            'class' => 'selectionsio-subsubmenu-link-class',
            'title' => 'Passwort vergessen Formular'
        ),
        'parent' => 'selectionsio-subsubmenu-link0',
    );
    $wp_admin_bar->add_menu($args);
    $args = array(
        'id'    => 'selectionsio-subsubmenu-link-4',
        'title' => 'Liefer & Rechnungsadresse-Formular',
        'href'  => '/wp-admin/admin.php?page=Selectionsioapi_formeditor_page&tab=deliveryandinvoiceform',
        'meta'  => array(
            'class' => 'selectionsio-subsubmenu-link-class',
            'title' => 'Liefer & Rechnungsadresse-Formular'
        ),
        'parent' => 'selectionsio-subsubmenu-link0',
    );
    $wp_admin_bar->add_menu($args);
    $args = array(
        'id'    => 'selectionsio-subsubmenu-link-5',
        'title' => 'Registrierformular',
        'href'  => '/wp-admin/admin.php?page=Selectionsioapi_formeditor_page&tab=registerform',
        'meta'  => array(
            'class' => 'selectionsio-subsubmenu-link-class',
            'title' => 'Registrierformular'
        ),
        'parent' => 'selectionsio-subsubmenu-link0',
    );
    $wp_admin_bar->add_menu($args);
    $args = array(
        'id'    => 'selectionsio-subsubmenu-link1',
        'title' => 'Aufträge',
        'href'  => '/wp-admin/admin.php?page=Selectionsioapi_order_page',
        'meta'  => array(
            'class' => 'selectionsio-subsubmenu-link-class',
            'title' => 'Aufträge'
        ),
        'parent' => 'selectionsio-submenu',
    );
    $wp_admin_bar->add_menu($args);
    $args = array(
        'id'    => 'selectionsio-subsubmenu-link2',
        'title' => 'Kunden',
        'href'  => '/wp-admin/admin.php?page=Selectionsioapi_customer_page',
        'meta'  => array(
            'class' => 'selectionsio-subsubmenu-link-class',
            'title' => 'Kunden'
        ),
        'parent' => 'selectionsio-submenu',
    );
    $wp_admin_bar->add_menu($args);
    $args = array(
        'id'    => 'selectionsio-subsubmenu-link3',
        'title' => 'Zahlarten',
        'href'  => '/wp-admin/admin.php?page=Selectionsioapi_payment_page',
        'meta'  => array(
            'class' => 'selectionsio-subsubmenu-link-class',
            'title' => 'Zahlarten'
        ),
        'parent' => 'selectionsio-submenu',
    );
    $wp_admin_bar->add_menu($args);
    $args = array(
        'id'    => 'selectionsio-subsubmenu-link4',
        'title' => 'Versandarten',
        'href'  => '/wp-admin/admin.php?page=Selectionsioapi_shipping_page',
        'meta'  => array(
            'class' => 'selectionsio-subsubmenu-link-class',
            'title' => 'Versandarten'
        ),
        'parent' => 'selectionsio-submenu',
    );
    $wp_admin_bar->add_menu($args);
}
}
add_action('admin_bar_menu', 'selectionsio_admin_link', 999);

function my_selectionsio_dashboard_widget() {
    echo '<div style="text-align:center;"><a href="/wp-admin/admin.php?page=Selectionsioapi-settings"><img src="'.plugins_url( 'images/selectionsio.png', __FILE__ ).'" /></a></div>';
    $subscriber_count = count_users();
    $subscriber_count = $subscriber_count['avail_roles']['psc_role'];
    echo "<p>Anzahl Kunden: " . $subscriber_count . "</p>";
    global $wpdb;
    $table_name = $wpdb->prefix . "Selectionsioapi_formplugin where `id` = 1";
    $results = $wpdb->get_results( "SELECT * FROM $table_name" );
    if($results[0]->registerform == "firstdata") {
echo '<label><a style="color:red;font-weight:500;" href="/wp-admin/admin.php?page=Selectionsioapi_formeditor_page&tab=registerform">Bitte Registrien-Formular anlegen!</a></label><br />';
    }
    if($results[0]->deliveryandinvoiceform == "firstdata") {
echo '<label><a style="color:red;font-weight:500;" href="/wp-admin/admin.php?page=Selectionsioapi_formeditor_page&tab=deliveryandinvoiceform">Bitte Liefer & Rechnungsadresse-Formular anlegen!</a></label><br />';
    }
    if($results[0]->loginform == "firstdata") {
echo '<label><a style="color:red;font-weight:500;" href="/wp-admin/admin.php?page=Selectionsioapi_formeditor_page&tab=loginform">Bitte Login-Formular anlegen!</a></label><br />';
    }
    if($results[0]->pwlostform == "firstdata") {
echo '<label><a style="color:red;font-weight:500;" href="/wp-admin/admin.php?page=Selectionsioapi_formeditor_page&tab=pwlostform">Bitte Passwort vergessen Formular anlegen!</a></label><br />';
    }
}

function add_my_selectionsio_dashboard_widget() {
    wp_add_dashboard_widget('my_selectionsio_dashboard_widget', 'Selections/IO Plugin ist Aktiv', 'my_selectionsio_dashboard_widget');
}
add_action('wp_dashboard_setup', 'add_my_selectionsio_dashboard_widget');
