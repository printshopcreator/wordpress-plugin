<?php
function Selectionsioapi_payment_page() {
$urlAPI = esc_attr( get_option('sio_api_link') ) . "/apps/api/";
//$allAccountsID->getApiToken();


$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => $urlAPI . "payment",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
  CURLOPT_HTTPHEADER => array(
    'Accept: application/json'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
//\var_dump($response);
$result = json_decode($response);

//var_dump($result->data);
?>
<style>
.SIO-logo {
    position: relative;
    right: 20px;
    top: 50%;
    width: 313px;
    height: 80px;
    background: url(<?php echo plugins_url( 'images/selectionsio.png', __FILE__ ); ?>) center top/313px 63px no-repeat;
}
.SIO-version {
    position: absolute;
    width: 100%;
    bottom: 0;
    text-align: center;
    color: #72777c;
    line-height: 1em;
}
#textarea, #textarea2 {
    width: 400px;
}
</style>
<div class="wrap">
    <a target="_blank" href="https://Selectionsio.de/"><div class="SIO-logo">
			<div class="SIO-version">Selectionsio Produkt-Konfigurator v.1.0.0</div>
		</div></a>
        <h1>Selectionsio Zahlarten</h1>
        <div class="body">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th><a class="sortable" title="Uid">Uid</a>
</th>
                        <th><a class="sortable" title="Name">Name</a>
</th>
                        <th><a class="sortable" title="Kosten">Kosten</a>
</th>
                        <th><a class="sortable" title="Kosten">MwSt.</a>
</th>
                    </tr>
                    </thead>
                    <tbody>
<?php foreach($result->data as $paymens) { ?>
                      <tr class="color">
                            <td><?php echo $paymens->uid; ?></td>
                            <td><?php echo $paymens->title; ?></td>
                            <td><?php echo $paymens->price; ?></td>
                            <td><?php echo $paymens->taxClass/10; ?>%</td>
                        </tr>
<?php } ?>
                                        </tbody>
                </table>
                <div class="navigation">
                    
                </div>
            </div>
</div>
<?php } ?>