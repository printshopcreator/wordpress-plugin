<?php
function Selectionsioapi_customer_page() {
    ?>
<style>
.SIO-logo {
    position: relative;
    width: 313px;
    height: 80px;
    background: url(<?php echo plugins_url( 'images/selectionsio.png', __FILE__ ); ?>) center top/313px 63px no-repeat;
}
.SIO-version {
    position: absolute;
    width: 100%;
    bottom: 0;
    text-align: center;
    color: #72777c;
    line-height: 1em;
}
#textarea, #textarea2 {
    width: 400px;
}
@media only screen and (max-width: 600px) {
        table {
            width: 100%;
        }
        td, th {
            display: block;
            width: auto;
            text-align: left;
        }
    }
</style>
    <div class="wrap">
        <a target="_blank" href="https://Selectionsio.de/"><div class="SIO-logo">
                <div class="SIO-version">Selectionsio Produkt-Konfigurator v.1.0.0</div>
            </div></a>
            <h1>Selectionsio Kundenübersicht</h1>
    </div>
    <?php
if(!isset($_GET["showaddresses"])) {
$curl = curl_init();
$urlAPI = esc_attr( get_option('sio_api_link') ) . "/apps/api/contact/allbyshop/" .  esc_attr( get_option('sio_shop_uuid') );
curl_setopt_array($curl, array(
  CURLOPT_URL => $urlAPI,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
  CURLOPT_HTTPHEADER => array(
    'Authorization: Bearer '. esc_attr( get_option('sio_api_token') ).''
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
}

if(isset($_GET["showaddresses"])) {
echo"<h2>Rechnungsadressen Übersicht:</h2>";
$curl = curl_init();
$urlAPI = esc_attr( get_option('sio_api_link') ) . "/apps/api/contact/address/getallfortype/".$_GET["customerUUID"]."/1";
curl_setopt_array($curl, array(
  CURLOPT_URL => $urlAPI,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
  CURLOPT_HTTPHEADER => array(
    'Authorization: Bearer '. esc_attr( get_option('sio_api_token') ).''
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$result = json_decode($response);
foreach($result->data as $key => $invoiceaddressarray) {
    echo '<div style="border: 1px solid;padding-right:20px;padding-left:20px;padding-bottom:20px;margin-right:20px;width:auto;float:left; width:400px;">';
    echo "<h3>Rechnungsadresse " . ($key + 1) . ".</h3>";
    echo "<table>";
    foreach($invoiceaddressarray as $key => $invoiceaddress) {
        echo "<tr>";
        echo "<td>" . $key . "</td>";
        echo "<td>" . $invoiceaddress . "</td>";
        echo "</tr>";
    }
    echo "</table>";
    echo "</div>";
}
echo '<div style="clear: both;"></div>';
echo"<h2>Lieferadressen Übersicht:</h2>";
$curl = curl_init();
$urlAPI = esc_attr( get_option('sio_api_link') ) . "/apps/api/contact/address/getallfortype/".$_GET["customerUUID"]."/2";
curl_setopt_array($curl, array(
  CURLOPT_URL => $urlAPI,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
  CURLOPT_HTTPHEADER => array(
    'Authorization: Bearer '. esc_attr( get_option('sio_api_token') ).''
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$result = json_decode($response);

foreach($result->data as $key => $deliveryaddressarray) {
    echo '<div style="border: 1px solid;padding-right:20px;padding-left:20px;padding-bottom:20px;margin-right:20px;width:auto;float:left; width:400px;">';
    echo "<h3>Lieferadresse " . ($key + 1) . ".</h3>";
    echo "<table>";
    foreach($deliveryaddressarray as $key => $deliveryaddress) {
        echo "<tr>";
        echo "<td>" . $key . "</td>";
        echo "<td>" . $deliveryaddress . "</td>";
        echo "</tr>";
    }
    echo "</table>";
    echo "</div>";
}
}
}
?>