<?php
function login_via_api($username, $password) {
    $urlAPI = esc_attr( get_option('sio_api_link') ) . "/apps/api/contact/existswithpassword";
    $data = array(
      'username' => $username,
      'password' => $password
    );
    $options = array(
      'http' => array(
          'method' => 'POST',
          'content' => json_encode($data),
          'header' => "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n" .
              'apiKey: 8a49e32c-a0fd-4aa3-9e79-6d7c1a5587aa'
      )
    );
   
    $context = stream_context_create($options);
    $result = file_get_contents($urlAPI, false, $context);
    $response = json_decode($result);
   //var_dump($response);
   if ($response->exists) {
    $user = get_user_by('login', $_POST['self_email']);
    if (!$user) {
      $user = wp_create_user($_POST['self_email'], $_POST['password'], $_POST['self_email']);
    }
    if (!is_wp_error($user) && $user) {
      $users = new WP_User( $user );
      $users->set_role( 'psc_role' );
        return true;
      } else {
        return false;
      }
  } else {
    ?>
Benutzername oder Passwort falsch.
    <?php
  }
}

  
function Selectionsio_my_login_form() {
    if (is_user_logged_in()) {
      return;
    }
    if (isset($_POST['self_email']) && isset($_POST['password'])) {
      $response = login_via_api($_POST['self_email'], $_POST['password']);
      if ($response) {
        $user = get_user_by('login', $_POST['self_email']);
        wp_set_current_user($user->ID, $_POST['self_email']);
        wp_set_auth_cookie($user->ID);
        do_action('wp_login', $_POST['self_email']);
        wp_redirect(home_url());
        exit;
      }
    }
    global $wpdb;
    $table_name = $wpdb->prefix . "Selectionsioapi_formplugin where `id` = 1";
    $results = $wpdb->get_results( "SELECT * FROM $table_name" );
    echo "<style>" .  $results[0]->formcssdata . "</style>";
    ?>
    <form name="loginform" action="" method="post">
<?php echo $results[0]->loginform; ?>
    </form>
    <?php
  }
  
  

  function Selectionsio_my_login_form_shortcode() {
    ob_start();
    Selectionsio_my_login_form();
    return ob_get_clean();
  }
  add_shortcode( 'Selectionsio_my_login_form', 'Selectionsio_my_login_form_shortcode' );


  function lostpw_via_api($username, $password) {
    $urlAPI = esc_attr( get_option('sio_api_link') ) . "/apps/api/contact/existswithpassword";
    $data = array(
      'username' => $username,
      'password' => $password
    );
    $options = array(
      'http' => array(
          'method' => 'POST',
          'content' => json_encode($data),
          'header' => "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n" .
              'apiKey: 8a49e32c-a0fd-4aa3-9e79-6d7c1a5587aa'
      )
    );
   
    $context = stream_context_create($options);
    $result = file_get_contents($urlAPI, false, $context);
    $response = json_decode($result);
   //var_dump($response);
   if ($response->exists) {
    $user = get_user_by('login', $_POST['self_email']);
    if (!$user) {
      $user = wp_create_user($_POST['self_email'], $_POST['password'], $_POST['self_email']);
    }
    if (!is_wp_error($user) && $user) {
      $users = new WP_User( $user );
      $users->set_role( 'psc_role' );
        return true;
      } else {
        return false;
      }
  } else {
    ?>
Benutzername oder Passwort falsch.
    <?php
  }
}

  
function Selectionsio_my_lostpw_form() {
    if (is_user_logged_in()) {
      return;
    }
    if (isset($_POST['self_email']) && isset($_POST['password'])) {
      $response = login_via_api($_POST['self_email'], $_POST['password']);
      if ($response) {
        $user = get_user_by('login', $_POST['self_email']);
        wp_set_current_user($user->ID, $_POST['self_email']);
        wp_set_auth_cookie($user->ID);
        do_action('wp_login', $_POST['self_email']);
        wp_redirect(home_url());
        exit;
      }
    }
    global $wpdb;
    $table_name = $wpdb->prefix . "Selectionsioapi_formplugin where `id` = 1";
    $results = $wpdb->get_results( "SELECT * FROM $table_name" );
    echo "<style>" .  $results[0]->formcssdata . "</style>";
    ?>
    <form name="pwlostform" action="" method="post">
<?php echo $results[0]->pwlostform; ?>
    </form>
    <?php
  }
  

  function Selectionsio_my_lostpw_form_shortcode() {
    ob_start();
    Selectionsio_my_lostpw_form();
    return ob_get_clean();
  }
  add_shortcode( 'Selectionsio_my_lostpw_form', 'Selectionsio_my_lostpw_form_shortcode' );